% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [has_desc,dim,ind_reduced_list_out,list_index] = get_has_descendant(current_index, indizes_list, ind_reduced_list)
%% get_has_descendant: 
%       search through the list of indizes if a current index 
%       has a descendant in the N-D wavelet tree
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           04.12.2019
%   last Revision:  25.02.2020
%
%   Input:  current_index   ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%                                 stage >=0
%                                 0 <= subband <= (2^stage - 1)
%
%           indizes_list    ...  (M x 1) cell array with (nDim x 2) integers
%                                inlcuding all indizes of the tree
%           ind_reduced_list...  (array) [int] list with relevant indizes
%                                (sub set of the indizes_list)
%
%   Output: has_desc       ...  [bool] returns true if current_index has a
%                                descendant in the specified N-D tree
%           dim             ...  (1 x dims) [int] returns the dimensions to split
%                                if no split is to be done, returns an
%                                empty array
%           ind_reduced_list_out  ...  (array) [int] reduced list with relevant indizes
%                                      (sub set of the indizes_list)
%           list_index      ...  [int] list position of the current index
%                                
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get parameters
nIndizes = length(indizes_list);
nDim = length( indizes_list{1}(:,1) );
list_index = 0;

% set default for reduced list indizes to full list
if nargin < 3
    ind_reduced_list = 1:nIndizes;
end

% set default, if no descendant is found
has_desc=false;
if ~isempty(ind_reduced_list)
    necessary_dims = false( nDim,1 );
    allowed_dims = true(nDim,1);
end
ind_reduced_list_out = zeros(1,nIndizes);

% check all list elements
for n = ind_reduced_list
    temp_ind = indizes_list{n};
    
    % exclude the parent index
    if all(current_index(:,1) <= temp_ind(:,1) ) && any(current_index(:,1) < temp_ind(:,1))
        
        % calculate traceback of the temp_ind knot
        traceback = floor (temp_ind(:,2)./(2.^(temp_ind(:,1)-current_index(:,1))) );
        
        % compare traceback with current index    
        if all(traceback == current_index(:,2))
            
            % set output to true
            has_desc = true;
            ind_reduced_list_out(n) = n;
            
            % check if split in the different dimensions is allowed and/or
            % necessary
            necessary_dims = necessary_dims |   (current_index(:,1) ~= temp_ind(:,1));
            allowed_dims = allowed_dims     &   (current_index(:,1) ~= temp_ind(:,1));
            
        end
    end
    
    if isequal(current_index,temp_ind)
        ind_reduced_list_out(n) = n;
        list_index = n;
    end
end

if ~isempty(ind_reduced_list)
    % find a single necessary and allowed dimension for the next split
    dim = find(necessary_dims & (allowed_dims) , 1);
    
    % if no allowed dimension is found, split all necessary dimensions
    if isempty(dim)
        dim = find(necessary_dims & (~allowed_dims));
    end
else
    dim=[];
end

ind_reduced_list_out(ind_reduced_list_out==0) = [];

end


