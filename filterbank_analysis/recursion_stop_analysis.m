% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [split_next] = recursion_stop_analysis(in_coef, has_desc, next_dim_vec, current_index, filter)
%% recursion_stop_analysis:  
%       Calculate the configurable metaData of the current coefficient and decide if the 
%       tree should be further split.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           05.12.2019
%   last Revision:  13.05.2020
%
%   Input:  in_coef      ...  cell array (1 x 2^N) with tree coefficients [N x M x ...]
%           has_desc    ...  [bool] knot has a descendant in the list
%           next_dim_vec ...  (1 x dims) dimensions of the next split to be done 
%           current_index...  (nDim x 2)
%                                 dim 1   | stage  ,  subband|
%                                 dim 2   | stage  ,  subband|
%                                 dim 3   | stage  ,  subband|
%           filter      ...  (cell array) filter coefficients
%
%   Output: split_next  ...  (1 x dims)[int] returns true if current_index is
%                            allowed to be split in the specified
%                            dimensions
%
%   split_next is false, if the number of coefficients in the specified 
%   dimension is shorter than the filter length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
split_next = [];

if has_desc
    
    for n=1:length(next_dim_vec)
        
        next_dim = next_dim_vec(n);
        % get filter maximal lengths
        max_elen = max([filter{next_dim}{1}{1}{3},filter{next_dim}{1}{2}{3},...
            filter{next_dim}{2}{1}{3},filter{next_dim}{2}{2}{3}]);
        
        % stopping exception resulting signal is to short
        if (max_elen > size(in_coef{1}{1,1},next_dim))
            [~, warnId] = lastwarn;
            if ~isequal(warnId,'AWPT:Split:SignalShort')
                warning('AWPT:Split:SignalShort',['Signal is too short. Stopping the splitting at stage=[',num2str(current_index(:,1)'),'].']);
            end
        else
            split_next(end+1) = next_dim;
        end
        
    end
end

 
end

