% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coefs] = recursion_start_analysis(signal, fil, split_param)
%% recursion_start_analysis:  
%       calculates the first step of the N-D tree.
%       This function represents the entry point of the recursion.
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           04.12.2019
%   last Revision:  03.06.2020
%
%   Input:  signal      ...  (1 x nBatches) cell array of signal [M x N x ...]
%           fil         ...  (1 x dim)(1x4) (cell array) filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%           split_param ...  struct with parameters which stages and
%                            subbands to use
%                           
%   Output: coefs       ... (N x (4+2^N) cell matrix) coefficients of the WPT
%                           {
%                           [stage, subband],
%                           support frequencies [fx,...fz],
%                           dual tree description
%                              [Re   Re  ..  Im
%                               ..   ..      ..
%                               Re   Im  ..  Im]
%                           Tree 1    (Re...Re),
%                           Tree 2    (Re...ReIm),
%                           ...
%                           Treee 2^N (Im...Im),
%                           metaData
%                           }
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% preallocate memory

% local variables
nBatches = length(signal);
in_coef = cell(1,nBatches);

% define results variables as global to prevent memory overflow due to redundancy
global result coef_list;
result = cell(1,nBatches);
coef_list = split_param.coef_indizes;


%% set start states
nDim = size(coef_list{1},1);
current_index = 0*coef_list{1};
mem = ones(1,nDim);


%% get real WP and AWP dimensions
% which dimension is WPT
is_realWPT = cellfun(@(x) isequal(x{1}{1},x{1}{2}),fil);
ReIm_pattern = de2bi(0:2^nDim-1,'left-msb');

% delete all imaginary trees of real WPT
ReIm_pattern(any(ReIm_pattern(:,is_realWPT)==1,2),:)=[];
split_param.ReImPattern = ReIm_pattern;

% create string to describe tree compositions
ReIm_pattern_string = strings(size(ReIm_pattern));
ReIm_pattern_string(:) = "Re";
ReIm_pattern_string(ReIm_pattern==1) = "Im";

% norm the combined trees signal energy to 1
signal = cellfun(@times,signal,num2cell(1/sqrt(size(ReIm_pattern,1))*ones(1,nBatches)),'un',0);


%% signal split into the necessary crossterms
% create cells for different trees
for idx = 1:nBatches
    in_coef{1,idx} = cell(1,size(ReIm_pattern,1));
    for n=1:size(ReIm_pattern,1)
        in_coef{idx}{1,n} = signal{idx};
    end
end

% create output structure
for idx=1:nBatches
    result{idx} = cell(0,4+size(ReIm_pattern,1));
end


%% get properties of current index
% next dimension or stopping condition
[has_desc, dim_vec, split_param.ind_reduced_list,coef_ind] = get_has_descendant(current_index,coef_list,split_param.ind_reduced_list);
split = recursion_stop_analysis(in_coef, has_desc, dim_vec, current_index, fil);

% check if the current index is contained in the list
save_knot = coef_ind~=0;

if ~isempty(split)
    %% save current knot, if knot specified in indizes list
    if save_knot
        
        % remove coefficient from list
        coef_list{coef_ind} = -1*ones(nDim,2);

        % calculate coef costs (lower cost is better, can be negative)
        [metaData] = split_param.hMetaFun(in_coef ,split_param.MetaParameters);
        
        % calculate subband frequency
        frequency = 1./(2.^(current_index(:,1)+1)) .*(current_index(:,2)+0.5);
        
        if split_param.OnlyMetaData
            % save results for all batches
            for idx=1:nBatches
                result{idx}(end+1,:) = [{current_index},...
                {frequency},...
                {ReIm_pattern_string.'},...
                cell(1,size(ReIm_pattern,1)),...
                {metaData}];
            end
        else
            % save results for all batches
            for idx=1:nBatches
                result{idx}(end+1,:) = {current_index,...
                    frequency,...
                    ReIm_pattern_string.',...
                    in_coef{idx}{1,:},...
                    metaData};
            end
        end
        
    end
    
    for n_dim=1:length(split)
        
        %% further splitting, begin recursion
        dim = split(n_dim);
        [lowCoef, highCoef] = prepare_filtering_analysis(in_coef, current_index, fil,mem, dim, ReIm_pattern);
        
        % lowpass
        % update [current_index, mem] in the next iteration
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)];
        mem(dim) = 1;
        % split next knot
        recursion_step_analysis(lowCoef,next_index,fil,mem,split_param);
        
        
        % bandpass
        % update [current_index, mem] in the next iteration
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)+1];
        mem(dim) = 2;
        % split next knot
        recursion_step_analysis(highCoef,next_index,fil,mem,split_param);
    end
    
else
    %% stop splitting
    
    % remove coefficient from list
    if coef_ind>0
        coef_list{coef_ind} = -1*ones(nDim,2);
    end
    
    % calculate coef costs (lower cost is better, can be negative)
    [metaData] = split_param.hMetaFun(in_coef ,split_param.MetaParameters);
    
    % calculate subband frequency
    frequency = 1./(2.^(current_index(:,1)+1)) .*(current_index(:,2)+0.5);
    
    if split_param.OnlyMetaData
        % save results for all batches
        for idx=1:nBatches
            result{idx}(end+1,:) = [{current_index},...
                {frequency},...
                {ReIm_pattern_string.'},...
                cell(1,size(ReIm_pattern,1)),...
                {metaData}];
        end
    else
        % save results for all batches
        for idx=1:nBatches
            result{idx}(end+1,:) = {current_index,...
                frequency,...
                ReIm_pattern_string.',...
                in_coef{idx}{1,:},...
                metaData};
        end
    end
    
    
end

coefs=result;

end
