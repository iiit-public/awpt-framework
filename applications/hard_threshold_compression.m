% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef] = hard_threshold_compression(coef,K,mode)
%% hard_treshold_compression:
%  Sorts the coefficients and removes the smallest coefficients to reach the
%  specified Compression Ratio. The absolute value is used as a default for
%  the sorting algorithm.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.01.2020
%   last Revision:  03.06.2020
%
% Input:
%       coef    ...     structure resulting from
%                       wavelet_packet_decomposition
%       K       ...     number of compressed coefficients
%       mode    ...     [optional]: 'analytic', 'real', 'single'
%                       specifies if the real or analytic coefficients
%                       are used as foundation or if only a single tree is
%                       used for compression
%
% Output:
%       coef ...        same structure as coef  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set defaul values
if nargin < 3
    mode='single';
end

if ~strcmp(mode,'single') && ~strcmp(mode,'analytic') && ~strcmp(mode,'real')
    warning('AWPT:Compression:Mode','Unknown compression mode. Use ''analytic'', ''real'' or ''single''. Fallback to ''single'' mode.');
    mode = 'single';
end
    

% check if input is structured in batches
if iscell(coef{1,1})
    nBatches = size(coef,2);
    calcInBatches = true;
else
    nBatches = 1;
    coef = {coef};
    calcInBatches = false;
end

% get number of trees and subbands
nTrees = size(coef{1},2)-4;
nSubbands = size(coef{1},1);

% catch too less trees error
if nTrees==1
    mode = 'single';
end

% calc tree combinations depending on the mode
if ~strcmp(mode,'single')
    [coef,A] = get_analytic_coefficients(coef,mode);
end

% calc for all batches
for n=1:nBatches
    
    % unpack signal
    coef_temp = coef{n};
    
    % read sizes, reshape to row vector and compensate energy
    switch mode
        case 'analytic'
            coef_temp = reshape(coef_temp(:,4:4+nTrees-1),1,[]);
            coef_sizes = cellfun(@size,coef_temp,'un',0);
            coef_shaped = cellfun(@(x) reshape(x,1,[]),coef_temp,'un',0);
        case 'real'
            coef_temp = reshape(coef_temp(:,4:4+nTrees/2-1),1,[]);
            coef_sizes = cellfun(@size,coef_temp,'un',0);
            coef_shaped = cellfun(@(x) 2*reshape(x,1,[]),coef_temp,'un',0);
        case 'single'
            coef_sizes = cellfun(@size,coef_temp(:,4),'un',0).';
            coef_shaped = cellfun(@(x) nTrees*reshape(x,1,[]),coef_temp(:,4),'un',0).';
    end

    % read sizes and concatenate
    coef_lengths = cellfun(@length,coef_shaped);
    coef_concatenated = cell2mat(coef_shaped);
    
    % create sorting order by ascending absolute value
    [~,sort_ind] = sort(abs(coef_concatenated),'descend');
    [~,rev_sort_ind] = sort(sort_ind);
    
    %% sort data -> cut of smallest elements -> sort back
    % sort
    coef_compressed = coef_concatenated(sort_ind); 
    
    % cut
    coef_compressed(K+1:end) = 0;
    
    % sort back
    coef_compressed = coef_compressed(rev_sort_ind);

    % create cell structure once again
    coef_compressed = mat2cell(coef_compressed,1,coef_lengths);

    % inverse reshape of signals back to N-D signal
    coef_compressed= cellfun(@(x,y) reshape(x,y), coef_compressed, coef_sizes, 'un',0);
    
    % pack signal and set the other coefficients to zero
    switch mode
        case 'analytic'
            % use coefficients of all trees
            coef_compressed = reshape(coef_compressed,nSubbands,nTrees);
            coef{n}(:,4:4+nTrees-1) = coef_compressed;
        case 'real'
            % use coefficients of half of the trees
            coef_compressed = reshape(coef_compressed,nSubbands,nTrees/2);
            coef{n}(:,4:4+nTrees/2-1) = coef_compressed;
            coef{n}(:,4+nTrees/2:end-1) = cellfun(@(x) x.*0,coef{n}(:,4+nTrees/2:end-1),'un',0);
        case 'single'
            % use coefficients of a single tree
            coef{n}(:,4) = coef_compressed;
            if nTrees>1
                coef{n}(:,5:end-1) = cellfun(@(x) x.*0,coef{n}(:,5:end-1),'un',0);
            end
    end
    
end

% calc inverse tree combinations depending on the mode
if ~strcmp(mode,'single')
    coef = get_tree_coefficients(coef,A);
end

% create output structure depending on the input structure
if ~calcInBatches
    coef = coef{1};
end

end