% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [sigma] = MAD_noise_estimation(sig,subband,a_fil)
%% MAD_noise_estimation:
%       estimates the noise level using the median of absolute deviation of the 
%       fine scale detail coefficients
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           01.07.2020
%   last Revision:  01.07.2020
%
% Input:
%       sig           ...     input N-D signal
%       subband       ...     the subband to use for noise estimation 
%                               (nDim x 2)
%                                   dim 1   | stage  ,  subband|
%                                   dim 2   | stage  ,  subband|
%                                   dim 3   | stage  ,  subband|
%       a_fil         ...     filter structure given by the function 'get_filters'
%                               specifies by which method the threshold is
%                               calculated. Default is VisuShrink ('Visu')     
%
% Output:
%       sigma         ...     the standard deviation of the estimated noise
%                             level
% See also: Donoho, D. L., De-noising by soft-thresholding, 
%           IEEE transactions of information theroy,1995
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set up minimal wavelet decomposition
split_param = get_decomposition_structure({subband},'preset_tree');

% perform AWPT analysis
coef = AWPT_analysis(sig,a_fil,split_param);

% find subband
[~,index] = is_in_coef_list(subband, coef(:,1));

Subband_data = coef{index,4};

% calculate noise via the MAD 
sigma = median(abs(Subband_data),'all')/0.6745;


end