% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode)
%% wavelet_denoising:
%  calculates the threshold, and uses hard- or soft-thresholding to reduce
%  the noise in the data.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           30.06.2020
%   last Revision:  01.07.2020
%
% Input:
%       coef            ...     structure resulting from
%                               wavelet_packet_decomposition
%       sigma           ...     the standard deviation of the noise
%       thres_estimation...     [optional]: 'Visu', 'Sure'
%                               specifies by which method the threshold is
%                               calculated. Default is VisuShrink ('Visu')
%       thres_mode      ...     [optional]: 'hard', 'soft'
%                               specifies the denoising method to use,
%                               hard-thresholding removes all coefficients smaller
%                               than the threshold, while soft-thresholding
%                               additionally shrinks the remaining coefficients in
%                               direction zero. Default is 'hard'

%       wavelet_mode    ...     [optional]: 'analytic', 'real', 'single'
%                               specifies if the real or analytic coefficients
%                               are used as foundation or if only a single tree is
%                               used for compression

%       
%
% Output:
%       coef            ...     same structure as coef  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set defaul values
if nargin < 5,   wavelet_mode='single';end
if nargin < 4,   thres_mode = 'hard';end
if nargin < 3,   thres_estimation = 'Visu';end


% catch wrong input parameters
if ~strcmp(thres_mode,'hard') && ~strcmp(thres_mode,'soft')
    warning('AWPT:Denoising:Mode','Unknown thresholding mode. Use ''hard'' or ''soft''. Fallback to ''hard'' mode.');
    thres_mode = 'hard';
end
if ~strcmp(thres_estimation,'Visu') && ~strcmp(thres_estimation,'Sure')
    warning('AWPT:Denoising:Mode','Unknown threshold estimation method. Use ''Visu'' or ''Sure''. Fallback to ''Visu'' mode.');
    thres_estimation = 'Visu';
end
if ~strcmp(wavelet_mode,'single') && ~strcmp(wavelet_mode,'analytic') && ~strcmp(wavelet_mode,'real')
    warning('AWPT:Denoising:Mode','Unknown wavelet mode. Use ''analytic'', ''real'' or ''single''. Fallback to ''single'' mode.');
    wavelet_mode = 'single';
end

% translate thres_mode for function wthresh
if strcmp(thres_mode,'hard'), thres_mode='h';end
if strcmp(thres_mode,'soft'), thres_mode='s';end
    

% check if input is structured in batches
if iscell(coef{1,1})
    nBatches = size(coef,2);
    calcInBatches = true;
else
    nBatches = 1;
    coef = {coef};
    calcInBatches = false;
end


% get wavelet decomposition parameters like number of trees, subbands,...
nTrees = size(coef{1},2)-4;
nSubbands = size(coef{1},1);



% catch too less trees error
if nTrees==1
    wavelet_mode = 'single';
end

% calc tree combinations depending on the mode
if ~strcmp(wavelet_mode,'single')
    [coef,A] = get_analytic_coefficients(coef,wavelet_mode);
end

% calc for all batches
for n=1:nBatches
    
    % unpack signal
    coef_temp = coef{n};
    
    % get number of samples in the relevant dimensions
    stages = coef_temp{1,1}(:,1).';
    N = size(coef_temp{1,4});
    N = N(1:length(stages)).*(2.^stages);
    
    
    
    for k=1:nSubbands
        
        % get subband and stages of the node
        subbands = coef_temp{k,1}(:,2);
        
        % only shrink detail coefficients
        if any(subbands~=0)
          
            % combine the trees depending on the dual-tree mode
            switch wavelet_mode
                case 'analytic'
                    coef_sizes = cellfun(@size,coef_temp(k,4:4+nTrees-1),'un',0);
                    coef_shaped = cellfun(@(x) reshape(x,1,[]),coef_temp(k,4:4+nTrees-1),'un',0);
                    coef_lengths = cellfun(@length,coef_shaped);
                    coef_matrix = cell2mat(coef_shaped);
                case 'real'
                    coef_sizes = cellfun(@size,coef_temp(k,4:4+nTrees/2-1),'un',0);
                    coef_shaped = cellfun(@(x) 2*reshape(x,1,[]),coef_temp(k,4:4+nTrees/2-1),'un',0);
                    coef_lengths = cellfun(@length,coef_shaped);
                    coef_matrix = cell2mat(coef_shaped);
                case 'single'
                    coef_sizes = size(coef_temp{k,4});
                    coef_matrix = nTrees*reshape(coef_temp{k,4},1,[]);
            end
            
            % use the universal threshold as upper limit
            thres_visu = sigma*sqrt(2*log(prod(N)));
            
            % calculate the threshold
            switch thres_estimation
                case 'Visu'
                    T = thres_visu;
                case 'Sure'
                    T = SureShrink(coef_matrix,sigma,thres_visu);
            end
            
            
            switch wavelet_mode
                case 'analytic'
                    
                    % apply shrinking method (hard/soft) with threshold T
                    temp = wthresh(abs(coef_matrix),thres_mode,T)./abs(coef_matrix);
                    temp(isnan(temp)) = 0;
                    coef_matrix = coef_matrix.*temp;
                    
                    % restructure the data to fit the cell array
                    coef_matrix = mat2cell(coef_matrix,1,coef_lengths);
                    coef_matrix= cellfun(@(x,y) reshape(x,y), coef_matrix, coef_sizes, 'un',0);
                    
                    % use coefficients of all trees
                    coef{n}(k,4:4+nTrees-1) = coef_matrix;
                case 'real'
                    
                    % apply shrinking method (hard/soft) with threshold T
                    coef_matrix = wthresh(coef_matrix,thres_mode,T);
                    
                    % restructure the data to fit the cell array
                    coef_matrix = mat2cell(coef_matrix,1,coef_lengths);
                    coef_matrix= cellfun(@(x,y) reshape(x,y), coef_matrix, coef_sizes, 'un',0);
                    
                    % use coefficients of half of the trees
                    coef{n}(k,4:4+nTrees/2-1) = coef_matrix;
                    coef{n}(k,4+nTrees/2:end-1) = cellfun(@(x) x.*0,coef_matrix,'un',0);
                case 'single'
                    
                    % apply shrinking method (hard/soft) with threshold T
                    coef_matrix = wthresh(coef_matrix,thres_mode,T);
                    
                    % use coefficients of a single tree
                    coef{n}{k,4} = reshape(coef_matrix,coef_sizes);
                    if nTrees>1
                        coef{n}(k,5:end-1) = cellfun(@(x) x.*0,coef{n}(k,5:end-1),'un',0);
                    end
            end
            
            
        end
        
    end
    
end

% calc inverse tree combinations depending on the mode
if ~strcmp(wavelet_mode,'single')
    coef = get_tree_coefficients(coef,A);
end

% create output structure depending on the input structure
if ~calcInBatches
    coef = coef{1};
end

end






function T = SureShrink(X,sigma,T_visu)
%% SureShrink:
%  calculates the threshold using minimizing of Stein's unbiased risk estimator (SURE)
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           30.06.2020
%   last Revision:  30.06.2020
%
% Input:
%       X            ...     [1 x N] input data vector
%       sigma        ...     [1 x 1] standard deviation of the awgn
%       T_visu       ...     [1 x 1] the universal threshold also called
%                            Visu threshold
%       
%
% Output:
%       T            ...     the scalar threshold, which minimizes SURE  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sort the absolute values
X = sort(abs(X));

% get number of samples
N_ges = numel(X);

% cut the data to only contain values smaller than the universal threshold
X = X(X<=T_visu);
N = numel(X);

if N>0
    % evaluate SURE for every T in [X(1) .. X(N)]
    SURE = sigma^2 - 1/N_ges*(2*sigma^2*(1:N)-(cumsum(X.^2)+(N_ges-(1:N)).*(X.^2)));
    
    % arg min SURE(X,T)
    [~,min_idx] = min(SURE);
    T = X(min_idx);
else
    T = T_visu;
end


end