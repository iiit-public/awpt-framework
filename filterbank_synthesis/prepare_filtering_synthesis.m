% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function result = prepare_filtering_synthesis(lp_coef,bp_coef,current_index,fil, mem,dim,ReIm_pattern)
%% prepare_filtering_synthesis
%       combine the two bands to get the next level, 
%       based on only the real tree
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           17.12.2019
%   last Revision:  14.05.2020
%
%   Input:  lp_coef     ...  low frequency signals  [M x N x ...]
%           hp_coef     ...  high frequency signals [M x N x ...]
%           current_index   ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           fil         ...  (cell array) filter coefficients
%                               {dim 1/2/3}
%                               {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                               {real=1,imag=2}
%                               {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
%           mem         ...  (1 x nDims) vector of [1,2,3,-3]
%                             change the order of low- and high-band 
%                             according to weickert 
%                             1: real imag filter without exchange
%                             2: exchange real <-> imag
%                                         lp   <-> hp
%                             3: use only real filter without exchange
%                            -3: use only real filter, exchange lp <-> hp
%           dim         ...   in which dimension to filter
%           ReIm_pattern ...  [nTrees x nDims] pattern consisting of {0,1},
%                             specifying which tree need the real 
%                             or imaginary filter, respectively
%
%   Output: result      ...  complex output signal [M x 2N x ...]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% extract filter pairs depending on the stage
if current_index(dim,1) == 0
    fil = fil{dim}(1:2);
else
    fil = fil{dim}(3:4);
end

% preallocate memory
result = cell(1,length(lp_coef));

% without imaginary tree
switch mem(dim)
    case 1  % BP->BP, LP->LP, Re -> Real tree, Im->Im_tree
        
        % extract filter pairs (arrangement depending on mem
        fil_re{1} = fil{1}{1};
        fil_re{2} = fil{2}{1};
        fil_im{1} = fil{1}{2};
        fil_im{2} = fil{2}{2};
        
        for n=1:length(lp_coef)
            if ReIm_pattern(n,dim)==0
                result{1,n} = filtering_step_synthesis(lp_coef{1,n}, bp_coef{1,n}, fil_re, dim);
            else
                result{1,n} = filtering_step_synthesis(lp_coef{1,n}, bp_coef{1,n}, fil_im, dim);
            end
        end
    case 2  % LP->BP, BP->LP, Im -> Real tree, Re->Im _tree
        
        % extract filter pairs (arrangement depending on mem
        fil_re{1} = fil{1}{2};
        fil_re{2} = fil{2}{2};
        fil_im{1} = fil{1}{1};
        fil_im{2} = fil{2}{1};
        
        for n=1:length(lp_coef)
            if ReIm_pattern(n,dim)==0
                result{1,n} = filtering_step_synthesis(bp_coef{1,n}, lp_coef{1,n}, fil_re, dim);
            else
                result{1,n} = filtering_step_synthesis(bp_coef{1,n}, lp_coef{1,n}, fil_im, dim);
            end
        end
    case 3  % BP->BP, LP->LP, Re -> Real tree, Re->Im_tree
        
        % extract filter pairs (arrangement depending on mem
        fil_re{1} = fil{1}{1};
        fil_re{2} = fil{2}{1};
        fil_im{1} = fil{1}{1};
        fil_im{2} = fil{2}{1};
        
        for n=1:length(lp_coef)
            if ReIm_pattern(n,dim)==0
                result{1,n} = filtering_step_synthesis(lp_coef{1,n}, bp_coef{1,n}, fil_re, dim);
            else
                result{1,n} = filtering_step_synthesis(lp_coef{1,n}, bp_coef{1,n}, fil_im, dim);
            end
        end
    case -3 % LP->BP, BP->LP, Re -> Real tree, Re->Im_tree
        
        % extract filter pairs (arrangement depending on mem
        fil_re{1} = fil{1}{1};
        fil_re{2} = fil{2}{1};
        fil_im{1} = fil{1}{1};
        fil_im{2} = fil{2}{1};
        
        for n=1:length(lp_coef)
            if ReIm_pattern(n,dim)==0
                result{1,n} = filtering_step_synthesis(bp_coef{1,n}, lp_coef{1,n}, fil_re, dim);
            else
                result{1,n} = filtering_step_synthesis(bp_coef{1,n}, lp_coef{1,n}, fil_im, dim);
            end
        end
end


end