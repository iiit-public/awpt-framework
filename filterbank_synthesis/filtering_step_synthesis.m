% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function y = filtering_step_synthesis(lp_sig, bp_sig, fil,dim)
%% filtering_step_synthesis  
%       upsampling of lowpass and bandpass child with subsequent
%       filtering and addition to create parent data
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           16.12.2019
%   last Revision:  20.02.2020
%
%   Input:  lp_sig ...  low subband signal [M x N x ...]
%           bp_sig ...  high subband signal [M x N x ...]
%           fil    ...  filter coefficients (1 x 2)(1 x 4) cell array
%           dim    ...  in which dimension to filter
%
%   Output: y      ...  output signal [M x 2N x ...]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prepare support variables for assignment in dynamic dimensions
N_vec = size(lp_sig);
N_vec(dim) = 2*N_vec(dim);
N = N_vec(dim);
S.type = '()';
S.subs = repmat({':'},1,ndims(lp_sig));

% upsample signals
S.subs{dim} = 1:2:N;
lp_sig_zp = zeros(N_vec);
bp_sig_zp = zeros(N_vec);
lp_sig = subsasgn(lp_sig_zp,S,lp_sig);
bp_sig = subsasgn(bp_sig_zp,S,bp_sig);


% extract filters
lowpass_b       = fil{1}{1};
lowpass_a       = fil{1}{2};
lowpass_length  = fil{1}{3};
lowpass_mid     = fil{1}{4};

bandpass_b      = fil{2}{1};
bandpass_a      = fil{2}{2};
bandpass_length = fil{2}{3};
bandpass_mid    = fil{2}{4};

% delay between lp and hp has to be even
M = 2;
lowpass_mid = M*round(lowpass_mid/M);
bandpass_mid = M*round(bandpass_mid/M);

%% distinguish FIR <-> IIR
if lowpass_a==1
    % use FIR method  
    
    %% zeropadding in specified dimension
    % zeropadping lowpass
    S.subs{dim} = N+1:N+lowpass_length;
    lp_sig = subsasgn(lp_sig,S,0);
    % zeropadding bandpass
    S.subs{dim} = N+1:N+bandpass_length;
    bp_sig = subsasgn(bp_sig,S,0);
    
    
    %% filter lowpass signal
    lp_sig_filtered = filter(lowpass_b,lowpass_a,lp_sig, [], dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(lp_sig_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(lp_sig_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end = subsref(lp_sig_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    lp_sig_filtered = subsasgn(lp_sig_filtered,S, sig_beginning + sig_end);
    
    % take only the values 1:N from the filtered signal
    S.subs{dim} = 1:N;
    lp_sig_filtered = subsref(lp_sig_filtered,S);
    lp_sig_filtered = circshift(lp_sig_filtered,-length(lowpass_b)+1+lowpass_mid,dim);
    
    
    %% filter highpass signal
    bp_sig_filtered = filter(bandpass_b,bandpass_a,bp_sig, [],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(bp_sig_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(bp_sig_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end = subsref(bp_sig_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    bp_sig_filtered = subsasgn(bp_sig_filtered,S, sig_beginning + sig_end);
    
    % take only the values 1:N from the filtered signal
    S.subs{dim} = 1:N;
    bp_sig_filtered = subsref(bp_sig_filtered,S);
    bp_sig_filtered = circshift(bp_sig_filtered,-length(bandpass_b)+1+bandpass_mid,dim);
    
    % recombine both signal parts
    y = lp_sig_filtered+bp_sig_filtered;
    
else
    % use IIR method
    
    MEM = round((lowpass_mid+bandpass_mid)/2);
    
    %% zeropadding in specified dimension
    % zeropadping lowpass
    S.subs{dim} = N+1:N+lowpass_length + MEM;
    lp_sig = subsasgn(lp_sig,S,0);
    % zeropadding bandpass
    S.subs{dim} = N+1:N+bandpass_length + MEM;
    bp_sig = subsasgn(bp_sig,S,0);
    
    
    %% filter lowpass signal
    lp_sig_filtered = filter(lowpass_b,lowpass_a,lp_sig, [], dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(lp_sig_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(lp_sig_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end = subsref(lp_sig_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    lp_sig_filtered = subsasgn(lp_sig_filtered,S, sig_beginning + sig_end);
    
    % take only the values 1:N from the filtered signal
    S.subs{dim} = 1:N;
    lp_sig_filtered = subsref(lp_sig_filtered,S);
    lp_sig_filtered = circshift(lp_sig_filtered,-length(lowpass_b)+1+lowpass_mid,dim);
    
    
    
    %% filter highpass signal
    bp_sig_filtered = filter(bandpass_b,bandpass_a,bp_sig, [],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(bp_sig_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(bp_sig_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end = subsref(bp_sig_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    bp_sig_filtered = subsasgn(bp_sig_filtered,S, sig_beginning + sig_end);
    
    % take only the values 1:N from the filtered signal
    S.subs{dim} = 1:N;
    bp_sig_filtered = subsref(bp_sig_filtered,S);
    bp_sig_filtered = circshift(bp_sig_filtered,-length(bandpass_b)+1+bandpass_mid,dim);
    
    % recombine both signal parts
    y = lp_sig_filtered+bp_sig_filtered;
    
end

