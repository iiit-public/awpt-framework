% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coefs] = get_tree_coefficients(analytic_coefs, A)
%% AWPT_analysis
%       Calculates the (A)WPT of the signal.
%       Filters and decomposition structure are defined by 
%       "fil" and "split_param", respectively
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           13.05.2020
%   last Revision:  31.05.2020
%
%   Input:  analytic_coef...(N x (4+2^N) cell matrix) coefficients of the WPT
%                           {
%                           [stage, subband],
%                           support frequencies [fx,...fz],
%                           frequency support description
%                              [fx1   fx2  ...  fx2^N
%                               ...   ...       ...
%                               fz1   fz2  ...  fz2^N]
%                           Coefficients (fx>0, ..., f>0,),
%                           Coefficients (fx>0, ..., fy>0, fz<0,),
%                           ...
%                           Coefficients (fx<0, ..., fz<0,),
%                           metaData
%                           }
%           A       ....    Linear combinatorics matrix [2^N x 2^N]
%
%   Output: coef    ...    (N x (4+2^N) cell matrix) coefficients of the WPT
%                           {
%                           [stage, subband],
%                           support frequencies [fx,...fz],
%                           dual tree description
%                              [Re   Re  ..  Im
%                               ..   ..      ..
%                               Re   Im  ..  Im]
%                           Tree 1    (Re...Re),
%                           Tree 2    (Re...ReIm),
%                           ...
%                           Treee 2^N (Im...Im),
%                           metaData
%                           }
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% preprocessing

% get input structure
if iscell(analytic_coefs{1,1})
    nBatches = size(analytic_coefs,2);
    calcInBatches = true;
else
    nBatches = 1;
    analytic_coefs = {analytic_coefs};
    calcInBatches = false;
end


% get dimension of AWPs
analyticDims = log2(size(analytic_coefs{1},2)-4);
signalDims = sum(size(analytic_coefs{1}{1,4})>1);
nTrees = 2^analyticDims;

nSubbands = size(analytic_coefs{1},1);


% invert the unitary matrix by complex conjugate transpose
A_inv = A';


% iterate through batches and subbands
for n=1:nBatches
    for k=1:nSubbands
        
        % concatenate coefficient of trees into signalDims+1 dimension
        coefficient_mat = cat(signalDims+1,analytic_coefs{n}{k,4:end-1});
        
        for p=1:nTrees
            
            % get weight vector for frequency support
            weight_vector = A_inv(p,:).';
            % add signalDims preceding singleton dimensions
            weight_vector = permute(weight_vector, circshift(1:signalDims+1,-1));
            
            % calc matrix multiplication of coefficient and weight vector
            analytic_coefs{n}{k,3+p} = sum(bsxfun(@times, coefficient_mat,weight_vector),signalDims+1);
               
        end
        
    end
end



% if necessary, remove singleton cell matrix dimension
if calcInBatches
    coefs = analytic_coefs;
else
    coefs = analytic_coefs{1};
end