# AWPT Framework - N-D Analytic Wavelet Packet Transform

This framework provides wavelet packet decomposition (analysis) and reconstruction (synthesis) for arbitrary decomposition structures both for the standard real-valued as well as the complex-valued (dual-tree) case.
Hence, the provided framework unifies the standard discrete wavelet transform (DWT), its complex-valued analogon, the dual-tree complex wavelet transform (DT-CWT) as well as the (analytic) wavelet packet transform. Signal adapted bases can be achieved using an optimal basis search of the full frame, using a user specified cost functional. As an example, an entropy-based cost functional is provided.

For every dimension of the signal, different filters can be specified.
At the time, the framework only supports decomposition using FIR-filters.

Test and example scripts can be found in the ``test_scripts`` folder.

This framework is licensed under the GNU-GPL v3 license (see below).

If you use this framework in your scientific research, please cite our paper:

> M. Bächle, M. Schambach and F. Puente León, "Signal-Adapted Analytic Wavelet Packets in Arbitrary     Dimensions," 2020 28th European Signal Processing Conference (EUSIPCO), Amsterdam, 2021, pp. 2230-2234,    doi: 10.23919/Eusipco47968.2020.9287575.


## Using the framework

The framework provides mainly four functions to be used for signal decomposition and reconstruction:

* ``get_filters``:
    Returns the analysis and synthesis filters, ``a_fil`` and ``s_fil``,  which are needed by the corresponding analysis and synthesis functions.
    To perform complex-valued decomposition and reconstruction, two different filters have to be specified (the first filter will only be used in the first level and the second filter will be used in all subsequent levels).

* ``get_decomposition_structure``:
    Create the desired decomposition structure. This can be either pre-defined structures, such as the standard DWT ('wavelet_tree'), WPT ('full_tree') or full frame ('full_frame') decomposition, but also user defined structures ('preset_tree'), for example to perform a fully separable or anisotropic wavelet transform. The functions returns the ``split_param`` needed by the analysis and synthesis functions.

* ``AWPT_analysis``:
    Performs the (analytic) decomposition of the input signal as specified by the decomposition structure ``split_param`` and decomposition filters ``a_fil``.

* ``AWPT_synthesis``:
    Performs a reconstruction using the decomposition coefficients as specified by the decomposition structure ``split_param`` and synthesis filters ``s_fil``.

### Example for different filter designs
If two filters are specified, a complex-valued transform will be performed. Otherwise the first filter is used in all levels to perform a real-valued transform.
* real-valued transform:
	```matlab
	% use Daubechies-10 filters (db-1 ... db-45)
	[a_fil, s_fil] = get_filters('db',10,[],[]);
	
	% use Coiflets-5 (coif-1 ... coif-5)
	[a_fil, s_fil] = get_filters('coif',5,[],[]);
	
	% use Symlets-8 (sym-2 ... sym-45)
	[a_fil, s_fil] = get_filters('sym',8,[],[]);
	
	% use Intermediate Daubechies-Butterworth Scaling Filter (currently not reconstructable)
	[a_fil, s_fil] = get_filters('idabusfi',[],3,4);
	```
* complex-valued transform:
	```matlab
	% use Daubechies-1 filters in first level and hwlet-(3,4) with mid-phase factorization starting from second level
	[a_fil,s_fil] = get_filters('db',1,[],[],'hwlet',[],3,4,'mid_phase');
	
	% use Daubechies-1 filters in first level and bwlet-(3,4) with linear-phase factorization starting from second level
	[a_fil,s_fil] = get_filters('db',1,[],[],'bwlet',[],3,4,'lin_phase');
	
	% use Daubechies-1 filters in first level and qshift-6 starting from second level
	[a_fil,s_fil] = get_filters('db',1,[],[],'qshift',6,[],[],[]);
	```
	

### Example for 1-D signals
The framwork can process multiple 1-D signals at once. The data will be filtered in the direction of the first dimension.

* `AWPT_anlysis`: Perform 1-D dual-tree complex wavelet packet transform
    ```matlab
    nSignals = 10;      % Number of signals
    N = 8192;           % Number of samples
    Fs = 50e6;          % Sampling frequency
    Ts = 1/Fs;          % Sampling period
    t = 0:Ts:(N-1)*Ts;  % Time array

    signal = sin(2*pi*(700e3*ones(nSignals,1) + 20e6/(2*8192*2e-8)*t).*t);
    signal = signal./sqrt(sum(signal.^2,2));
    signal = signal';

    % create filters, one stage for real-valued transform
    [a_fil, s_fil] = get_filters('db',10,2,2);

    % create filters, two stages to obtain dual tree (complex-valued transform)
    % [a_fil, s_fil] = get_filters('db',10,2,2,'hwlet',20,1,6, 'min_phase');

    % specify tree structure (A-WPT) 1-D, 6 levels
    split_param = get_decomposition_structure([],'full_tree',1,6);

    % specify DT-CWT, 1-D, 6 levels
    % split_param = get_decomposition_structure([],'wavelet_tree',1,6);

    % user defined decomposition structure, 1D
    % split_param = get_decomposition_structure({[2,0],[2,1],[3,2],[3,3],[1,1]},'preset_tree',1);

    % perform decomposition
    [coef] = AWPT_analysis(signal, a_fil,split_param);
    ```

* `AWPT_synthesis`: Calculates the reconstruction of the signals
    ```matlab
    signal_reconstructed  = AWPT_synthesis(coef, s_fil);
    ```


### Example for N-D signals

* `get_decomposition_structure.m`:
	```matlab
    % Regular DWT in 2 dimensions, 3 levels
	split_param = get_decomposition_structure([],'wavelet_tree',2, 3);
	% or define own decomposition structure
	split_param = get_decomposition_structure({[1,0;0,0],[1,1;1,0],[1,1;1,1]},'preset_tree');
	```

* `AWPT_anlysis.m`:
	```matlab
	TestImage = double(imread('cameraman.tif'));
    TestImage = TestImage./max(TestImage,[],'all');
    
    % Regular wavelet transform in 2 dimensions, 2 levels
	split_param = get_decomposition_structure([],'wavelet_tree',2,2);
	
    % Get filters, dimension 1
    [a_fil{1},s_fil{1}] = get_filters('db',1,1,0,'hwlet',1,3,4,'mid_phase');
    
    % Get filters, dimension 2 (can differ from dimension 1)
    [a_fil{2},s_fil{2}] = get_filters('db',1,1,0,'hwlet',1,3,4,'mid_phase');
	
	[coef] = AWPT_analysis(TestImage, a_fil,split_param);
	
	% plot Results for 2D
	[matrix] = plot_WPT_2D(coef,0.8);
	```
* `AWPT_synthesis.m`:
    ```matlab
    % test image
    Image = double(imread('cameraman.tif'));
            
    % split parameter
    split_param = get_decomposition_structure([],'wavelet_tree',2,3);
            
    % filter design
    % dimension 1
    [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);

    % dimension 2
    [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
            
    % decomposition
    [coef] = AWPT_analysis(Image, a_fil,split_param);
            
    % reconstruction
    Image_rec = AWPT_synthesis(coef,s_fil);
    ```

## Filter visualisation
Have a look at the ``filter_visualisiation`` folder for easy filter and basis function visualization.


## Contribution
Contributions are welcome!
To contribute to this project, please fork our repository, create a new feature branch and create a merge request.
Please stick to the styleguide used throughout this project and comment new features or bug fixes correspondingly.


## License

Copyright (C) 2020  The AWP-Framework Authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
