% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [ND_data] = plot_SubbandsND(coef)
%% plot_SubbandsND
%       Create a concatenated array with all subband combinations existing
%       and plot plot it using the imshow function with gamma correction
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           22.06.2020
%   last Revision:  22.06.2020
%
%   Input:  coef        ...     coefficient data structure
%                               
%   Output: matrix      ...    gives back the pixels of the picture
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% check if Batches were used
if iscell(coef{1,1})
    warning('AWPT:PlotND:Batches','Multiple batches were given. I am going to use the first batch and ignore the following.');
    coef = coef{1,1};
end

% get resulting matrix size
nSize = size(coef{1,4});
stages = coef{1,1}(:,1).';
sizeMatrix = nSize;
sizeMatrix(1:length(stages)) = sizeMatrix(1:length(stages)).*(2.^(stages));

% preallocate memory
ND_data = zeros(sizeMatrix);

% place all coef_indizes inside the matrix
for n=1:size(coef,1)
    
    % get coef_index informations
    subband_data = coef{n,4};
    stages = coef{n,1}(:,1).';
    subbands = coef{n,1}(:,2).';

    % calc the position of the subband in the image
    delta = sizeMatrix(1:length(stages))./(2.^stages);
    PosStart = delta.*subbands+1;
    PosEnd = delta.*(subbands+1);
    
    % create the indexing statement
    S.type = '()';
    S.subs = cell(1,length(nSize));
    for k=1:nSize
        
        if k <= length(stages)
            S.subs{k} = PosStart(k):PosEnd(k);
        else
            S.subs{k} = ':';
        end
    end
    
    % asign subband to the ND-data matrix
    ND_data = subsasgn(ND_data,S,subband_data);
    
end


end