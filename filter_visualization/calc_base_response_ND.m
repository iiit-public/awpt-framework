% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [freq_resp,f_vec] = calc_base_response_ND(a_fil,subband_index,form,supp,Nx,f_vec)
%% calc_base_response_ND
%       calculates the response of AWP to a harmonics excitation
%       with specified frequencies
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           09.12.2019
%   last Revision:  09.06.2020
%
%   Input:  a_fil       ...  (1 x nDim)(1x4) (cell array) analysis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           subband_index    ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           supp     ...  [1,nTrees] Definition which frequency area 
%                           the base function should support (in the real
%                           form, only the first 1...nTrees/2 lead to
%                           different frequency areas, and then the
%                           supported areas will be repeated
%           form       ...  'real':     linear combination to get real
%                                       wavelets 
%                           'analytic': linear combination to get analytic
%                                       wavelets (default)
%           Nx         ...  number of samples of the harmonic excitation
%                           will be zeropadded to get a power of 2
%           f_vec      ...  (1 x K) vector of normalized frequencies
%                           (normalized by sampling rate) -> [-0.5, 0.5]
%
%
%   Output: freq_resp      ...   resulting (K x K x ...) response of the wavelet packet
%                          
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get filters maximal lengths
fil_lengths = cellfun(@(x) max([x{3}{1}{3},x{3}{2}{3},x{4}{1}{3},x{4}{2}{3}]), a_fil);

% variable arguments
if nargin < 3, form = 'analytic'; end
if nargin < 4, supp = 1; end
if nargin < 5
    Nx = max( fil_lengths.*(2.^subband_index(:,1).') );
else 
    Nx_min = fil_lengths.*(2.^subband_index(:,1).');
    Nx = max([Nx_min,Nx]);
    Nx = max( ceil( Nx./(2.^subband_index(:,1).') ).* (2.^subband_index(:,1).') );
end
if nargin < 6, f_vec = (-Nx/2:Nx/2-1)/Nx; end

% read dimensionality
dim = size(subband_index,1);
is_AWPT = cellfun(@(x) ~isequal(x{1}{1},x{1}{2}),a_fil);
nAnalyticDims = sum(is_AWPT);
nTrees = 2^nAnalyticDims;

% catch wrong support area
if supp>nTrees, supp=1; end

% preallocate memory
f_mesh   = cell(1,dim);
t_mesh = cell(1,dim);

% prepare meshgrid inputs
for n=1:dim
    f_mesh{n} = f_vec;
    t_mesh{n} = (0:Nx-1);
end

% create meshgrids
[f_mesh{:}] = ndgrid(f_mesh{:});
[t_mesh{:}] = ndgrid(t_mesh{:});

% save size of f_mesh
fDims = size(f_mesh{1});

% create array with all subband combinations
f_mesh = cellfun(@(x) reshape(x,1,[]), f_mesh,'un',0);
f_mesh = cell2mat(f_mesh.');

% prepare wavelet decomposition
split_param = get_decomposition_structure({subband_index},'preset_tree');

%% sweep through the subband combinations

% result reference
Sref.type = '()';
Sasgn.type= '()';
Sref.subs = num2cell(ones(1,dim));
Sasgn.subs = cell(1,size(f_mesh,1));


% preallocate memory
freq_resp = zeros(fDims);

for n=1:size(f_mesh,2)
    
    % create excitation signal
    argument = zeros(size(t_mesh{1}));
    for k=1:dim
        argument = argument + f_mesh(k,n)*t_mesh{k};
        Sasgn.subs{k} = find(f_mesh(k,n)==f_vec);
    end
    sig = exp(2i*pi*argument);

    % perform decomposition
    [coefs] = AWPT_analysis(sig,a_fil,split_param);
    coefs = get_analytic_coefficients(coefs,form);
    
    % read response from the correct coefficient
    [~,index] = is_in_coef_list(subband_index,coefs(:,1));
    temp = subsref(coefs{index,3+supp},Sref);
    freq_resp = subsasgn(freq_resp,Sasgn,temp);
    
end




end