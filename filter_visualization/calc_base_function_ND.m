% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [base] = calc_base_function_ND(s_fil,subband_index,form,supp,Nx)
%% calc_base_function_ND
%       calculates the base function corresponding to the 
%       specified coefficient of the AWP
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           19.11.2019
%   last Revision:  09.06.2020
%
%   Input:  s_fil      ...  (1 x nDim)(1x4) (cell array) synthesis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           subband_index    ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           form       ...  'real':     linear combination to get real
%                                       wavelets 
%                           'analytic': linear combination to get analytic
%                                       wavelets (default)
%           supp     ...  [1,nTrees] Definition which frequency area 
%                           the base function should support (in the real
%                           form, only the first 1...nTrees/2 lead to
%                           different frequency areas, and then the
%                           supported areas will be repeated
%           Nx         ...  number of samples of base function
%
%   Output: base       ...  resulting (1 x N) of the wavelet synthesis
%                           filterbank
%                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% read dimensionality
dim = size(subband_index,1);

%% set default values for variable arguments

% set base function sizes
if nargin < 5
    Nx = 1;
else
    Nx = 2^ceil(log2(Nx));
end

if nargin < 4
    supp=1;
end

% set form to analytic
if nargin < 3
    form = 'analytic';
end

% check cell structure of filter
if ~iscell(s_fil{1}{1}{1})
    s_fil= {s_fil};
end

% compare filter dimensions with subband dimensions
if length(s_fil)~=dim
    base=0;
    warning(['Wrong number of filters specified. Expected a cell array with ',num2str(dim),' filters'])
    return;
end
        
% calculate minimum allowed coefficient length
nCoeffs_min = zeros(1,dim);
for n=1:dim
    nCoeffs_min(n) = max([s_fil{n}{1}{1}{3},...
                          s_fil{n}{1}{2}{3},...
                          s_fil{n}{2}{1}{3},...
                          s_fil{n}{2}{2}{3}...
                          ]);
end

coef_Sizes = Nx./(2.^subband_index(:,1));
while(any((coef_Sizes*2) < nCoeffs_min))
    Nx = Nx *2 ;
    coef_Sizes = Nx./(2.^subband_index(:,1));
end

%% create coefficient structure

% get number of analytic dimensions
is_AWPT = cellfun(@(x) ~isequal(x{1}{1},x{1}{2}),s_fil);
nAnalyticDims = sum(is_AWPT);
nTrees = 2^nAnalyticDims;

% create coefficient vector
if length(coef_Sizes) ==1
    coef_dummy = zeros(coef_Sizes,1);
else
    coef_dummy = zeros(coef_Sizes.');
end
S.type = '()';
S.subs = num2cell(coef_Sizes/2);
coef_vec = subsasgn(coef_dummy,S,1);

% create all trees
coef_trees = cell(1,nTrees);
for n=1:nTrees
    if n==supp
        coef_trees{n} = coef_vec;
    else
        coef_trees{n} = coef_dummy;
    end
end

% fill structure with coefficients of interest
coef = {subband_index,zeros(dim,1),[],coef_trees{1,:},[]};

% calculate inverse recombination
[~,A] = get_analytic_coefficients(coef, form);
[coef] = get_tree_coefficients(coef, A);


%% reconstruct signal from coefficient structure
base  = AWPT_synthesis(coef, s_fil);


end

