% HWLET code by
% Ivan Selesnick, Polytechnic University, Brooklyn, NY
% selesi@taco.poly.edu
% and
% Markus Lang  <lang@dsp.rice.edu>
% Copyright: All software, documentation, and related files in this distribution
%            are Copyright (c) 1993  Rice University
%
% published in 
%   Ivan W. Selesnick, The design of approximate Hilbert transform pairs of wavelet bases,
%   IEEE Transaction on Signal Processing, 50(2): 1144-1152, 2002
%
% Permission is granted for use and non-profit distribution providing that this
% notice be clearly maintained. The right to distribute any portion for profit
% or as part of any commercial product is specifically reserved for the author.

function [h0_t, h0] = hwlet_bior(K1,K2,L,phi)
% Hilbert transform pair of orthogonal wavelet bases
% h, g - scaling filters of length 2*(K+L)
% K - number of zeros at z=-1
% L - degree of fractional delay
% phi - factorization mode: mid_phase, min_phase  
K=K1+K2;
n = 0:L-1;
t = 1/2;
d = cumprod([1, (L-n).*(L-n-t)./(n+1)./(n+1+t)]);
s1 = binom_coeff(K,0:K);
s2 = conv(d, d(end:-1:1));
s = conv(s1,s2);

N = K+1+2*L;
M = ceil(N/2);
C = convmtx(s',N);
C = C(1:2:end,:);
b = zeros(N,1);
b(M) = 1;
r = (C\b)';
if r ~= r(end:-1:1)
    disp('ERROR polynom r is not symmetrical')
else
    % disp('Polynom r is symmetrical')
end

switch phi
    case 'linphase'
        [q1,q2] = sfactS(r);
        if q2(end) == 0
            q2 = q2(1:end-1);  % delete the last zero 
        end
        if q1 ~= q1(end:-1:1)
            warning('Error Q1 is not symetric')
        else
            % disp('Q1 is symetric')
        end
        if q2 ~= q2(end:-1:1)
            warning('Error Q2 is not symetric')
        else
           %  disp('Q2 is symetric')
        end
            
    otherwise
        warning('Only lin_phase factorization available. Fallback to linphase factorization.');
        [q1,q2] = sfactS(r);
        if q2(end) == 0
            q2 = q2(1:end-1);  % delete the last zero 
        end
        if q1 ~= q1(end:-1:1)
            warning('Error Q1 is not symetric')
        else
            % disp('Q1 is symetric')
        end
        if q2 ~= q2(end:-1:1)
            warning('Error Q2 is not symetric')
        else
           %  disp('Q2 is symetric')
        end
end
f1 = conv(q1,binom_coeff(K1,0:K1));
h0 = conv(f1,d(end:-1:1));
f2 = conv(q2,binom_coeff(K2,0:K2));
h0_t = conv(f2,d);

 
 