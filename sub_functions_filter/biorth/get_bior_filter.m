% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag] = get_bior_filter(afil_lp_real,sfil_lp_real)
%% get_bior_filter    caclulates the filters as biorthogonal
%
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 14.11.2019 (Matthias Baechle)
%
%   Input: 
%          afil_lp_real   ...   analysis real tree lowpass filter   (1 x nFilterCoefs)
%          sfil_lp_real   ...   synthesis real tree lowpass filter  (1 x nFilterCoefs)
%
%   Output:
%          afil_lp_real   ...   analysis real tree lowpass filter   (1 x nFilterCoefs)
%          afil_bp_real   ...   analysis real tree bandpass filter  (1 x nFilterCoefs)
%          afil_lp_imag   ...   analysis imag tree lowpass filter   (1 x nFilterCoefs)
%          afil_bp_imag   ...   analysis imag tree bandpass filter  (1 x nFilterCoefs)
%          sfil_lp_real   ...   synthesis real tree lowpass filter  (1 x nFilterCoefs)
%          sfil_bp_real   ...   synthesis real tree bandpass filter (1 x nFilterCoefs)
%          sfil_lp_imag   ...   synthesis imag tree lowpass filter  (1 x nFilterCoefs)
%          sfil_bp_imag   ...   synthesis imag tree bandpass filter (1 x nFilterCoefs)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% get parameters
N1 = length(afil_lp_real);
N2 = length(sfil_lp_real);
n1 = 0:N1-1;
n2 = 0:N2-1;

%% create imaginary filters
afil_lp_imag = afil_lp_real(end:-1:1);
sfil_lp_imag = sfil_lp_real(end:-1:1);


%% create analysis highpass
% real tree
afil_bp_real =  ((-1).^n2).*sfil_lp_real;
sfil_bp_real = (-(-1).^n1).*afil_lp_real;
% imaginary tree
afil_bp_imag =  ((-1).^n2).*sfil_lp_imag;
sfil_bp_imag = (-(-1).^n1).*afil_lp_imag;

end