% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [afil_lp_real,afil_bp_real,sfil_lp_real,sfil_bp_real] = get_orth_cqf(afil_lp_real)
%% get_orth_cqf  creates conjugate quadrature filters
%
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 13.11.2019 (Matthias Baechle)
%
%   Input: afil_lp_real   ...   analysis lowpass filter (1 x nFilterCoefs) 
%
%   Output:
%          afil_lp_real   ...   analysis lowpass filter  (1 x nFilterCoefs)
%          afil_bp_real   ...   analysis highpass filter (1 x nFilterCoefs)
%          sfil_lp_real   ...   synthesis lowpass filter (1 x nFilterCoefs)
%          sfil_bp_real   ...   synthesis highpass filter (1 x nFilterCoefs)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


N = length(afil_lp_real);
n = 0:N-1;
% create analysis highpass
afil_bp_real = ((-1).^n).*afil_lp_real(end:-1:1);
% create synthesis lowpass
sfil_lp_real = afil_lp_real(end:-1:1);
% create synthesis highpass
sfil_bp_real = (-(-1).^n).*afil_lp_real;

end


