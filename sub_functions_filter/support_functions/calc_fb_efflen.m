% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil, s_fil] = calc_fb_efflen(a_fil,s_fil)
%% calc_fb_efflen   calculates the effective length of all the filters 
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 14.11.2019 (Matthias Baechle)
%
%   Input: a_fil   ...   cell array with analysis filters  
%                        {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denum=2}
%          s_fil   ...   cell array with synthesis filters  
%                        {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denum=2}
%
%   Output:
%          a_fil ...  cell array with analysis filters 
%                     {lowpass=1,bandpass=2}{real=1,imag=2}
%                       {coeffs_num=1,coeffs_denum=2,effectiveLength=3}
%          s_fil ...  cell array with synthesis filters
%                     {lowpass=1,bandpass=2}{real=1,imag=2}
%                       {coeffs_num=1,coeffs_denum=2,effectiveLength=3}
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% specify threshold for FIR and IIR filters
p_iir = 99;
p_fir = 100;

%% check if filter are FIR or IIR
% analysis filter
if length(a_fil{1}{1}{2})==1, a_fil_lp_re_fir = true; else,  a_fil_lp_re_fir = false; end
if length(a_fil{1}{2}{2})==1, a_fil_lp_im_fir = true; else,  a_fil_lp_im_fir = false; end
if length(a_fil{2}{1}{2})==1, a_fil_bp_re_fir = true; else,  a_fil_bp_re_fir = false; end
if length(a_fil{2}{2}{2})==1, a_fil_bp_im_fir = true; else,  a_fil_bp_im_fir = false; end
% synthesis filter
if length(s_fil{1}{1}{2})==1, s_fil_lp_re_fir = true; else,  s_fil_lp_re_fir = false; end
if length(s_fil{1}{2}{2})==1, s_fil_lp_im_fir = true; else,  s_fil_lp_im_fir = false; end
if length(s_fil{2}{1}{2})==1, s_fil_bp_re_fir = true; else,  s_fil_bp_re_fir = false; end
if length(s_fil{2}{2}{2})==1, s_fil_bp_im_fir = true; else,  s_fil_bp_im_fir = false; end


%% analysis filters

% lowpass real tree
lp_re= a_fil{1}{1};
if a_fil_lp_re_fir
    a_fil{1}{1}{3} = calc_efillen(lp_re, p_fir);
else
    a_fil{1}{1}{3} = calc_efillen(lp_re, p_iir);
end

% lowpass imaginary tree
lp_im = a_fil{1}{2};
if a_fil_lp_im_fir
    a_fil{1}{2}{3} = calc_efillen(lp_im, p_fir);
else
    a_fil{1}{2}{3} = calc_efillen(lp_im, p_iir);
end

% bandpass real tree
hp_re= a_fil{2}{1};
if a_fil_bp_re_fir
    a_fil{2}{1}{3} = calc_efillen(hp_re, p_fir);
else
    a_fil{2}{1}{3} = calc_efillen(hp_re, p_iir);
end

% bandpass imaginary tree
hp_im = a_fil{2}{2};
if a_fil_bp_im_fir
    a_fil{2}{2}{3} = calc_efillen(hp_im, p_iir);
else
    a_fil{2}{2}{3} = calc_efillen(hp_im, p_iir);
end


%% synthesis filters

% lowpass real tree
lp_re= s_fil{1}{1};
if s_fil_lp_re_fir
    s_fil{1}{1}{3} = calc_efillen(lp_re, p_fir);
else
    s_fil{1}{1}{3} = calc_efillen(lp_re, p_iir);
end

% lowpass imaginary tree
lp_im = s_fil{1}{2};
if s_fil_lp_im_fir
    s_fil{1}{2}{3} = calc_efillen(lp_im, p_fir);
else
    s_fil{1}{2}{3} = calc_efillen(lp_im, p_iir);
end

% bandpass real tree
hp_re= s_fil{2}{1};
if s_fil_bp_re_fir
    s_fil{2}{1}{3} = calc_efillen(hp_re, p_fir);
else
    s_fil{2}{1}{3} = calc_efillen(hp_re, p_iir);
end

% bandpass imaginary tree
hp_im = s_fil{2}{2};
if s_fil_bp_im_fir
    s_fil{2}{2}{3} = calc_efillen(hp_im, p_fir);
else
    s_fil{2}{2}{3} = calc_efillen(hp_im, p_iir);
end

end