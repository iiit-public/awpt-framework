% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil,s_fil] = get_fir_cqf(afil_lp_real)
%% get_fir_cqf   creates cell structure with the necessary filters 
%                for first stage FIR filters
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 13.11.2019 (Matthias Baechle)
%
%   Input: afil_lp_real   ...   analysis lowpass filter (1 x nFilterCoefs) 
%
%   Output:
%          a_fil ...  cell array with analysis filters 
%                     {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denom=2}
%          s_fil ...  cell array with synthesis filters
%                     {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denom=2}
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% insert zero at the end for symetric reconstruction in re and im tree
afil_lp_real = [afil_lp_real, zeros(1,2)];
% create imaginary filter with one sample delay condition
afil_lp_imag = [afil_lp_real(end), afil_lp_real(1:end-1)];

% calculate the corresponding bandpass filters and synthesis filters
[afil_lp_real,afil_bp_real,sfil_lp_real,sfil_bp_real] = get_orth_cqf(afil_lp_real);
[afil_lp_imag,afil_bp_imag,sfil_lp_imag,sfil_bp_imag] = get_orth_cqf(afil_lp_imag);

% pack the filters in cell array
[a_fil,s_fil]=pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag);

end