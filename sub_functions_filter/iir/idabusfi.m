% Copyright (C)
% Ivan W. Selesnick
% Rice University
% December, 1996


function [b,a,b1,b2] = idabusfi(M,N)
% IDABUSFI: Intermediate DAubechies-BUtterworth Scaling FIlter
%
% [b,a,b1,b2] = idabusfi(M,N);
% input 
%   M : number of zeros contributing to passband flatness
%   N : number of poles
%   need : M>=0; N>=0; N even
% output
%   b/a : IIR filter
%   b   : numerator coefficients
%   a   : denominator coefficients
%   b   : b = conv(b1,b2); b1 contains all zeros at z=-1,
%           b2 contains all other zeros.
%
%  % Examples  
%	% Daubechies-10
%	[b,a,b1,b2] = idabusfi(4,0);
%
%	% Classical Butterworth
%       [b,a,b1,b2] = idabusfi(0,4);
%
%	% Intermediate Daubechies-Butterworth
%       [b,a,b1,b2] = idabusfi(2,2);

% Ivan W. Selesnick
% Rice University
%
% required subprograms: maxflat2.m, choose.m


[b,a,b1,b2] = maxflat2(M+N+1,M,N);



