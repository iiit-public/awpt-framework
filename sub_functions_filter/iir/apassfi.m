% Copyright
% Ivan W. Selesnick
% Rice University

function [a1,a2,p,q] = apassfi(K,d)
% APASSFI: AllPAss Sum Scaling FIlter
% Design of a maximally flat lowpass scaling filter H(z) as the 
% sum of two allpass filters: H(z) = A1(z^2) + z^(-d) A2(z^2)
%
% [a1,a2,p,q] = apassfi(K,d);
% input 
%   K : degree of product A1(z) A2(z)
%   d : degree of delay
%   Note: two conditions must be satisfied
%     (1) 1 <= d <= 2K+1
%     (2) d must be odd
% output
%   a1, a2 : the denominators of the allpass filters A1(z), A2(z)
%   p/q : overall transfer function H(z)
%
%  % Examples  
%	% Approximately linear phase scaling filter
%	[a1,a2,p,q] = apassfi(3,5);
%
%	% Classical Butterworth
%       [a1,a2,p,q] = apassfi(3,1);
%
%	% General allpass sum scaling filter
%       [a1,a2,p,q] = apassfi(3,3);


% Ivan W. Selesnick
% Rice University
%
% required subprograms: maxflaps.m, flatdlay.m

% check input for validity:
b1 = (1 <= d) & (d <= 2*K+1);
b2 = rem(d,2)==1;
if ~(b1 && b2)
   warning('For this K, d must be one of the following:');
   warning(1:2:(2*K+1));
   return
end

[a1,a2,p,q] = maxflaps(K,K,d);
a1 = a1(1:2:length(a1));
a2 = a2(1:2:length(a2));


