% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef] = AWPT_analysis(signal, fil,split_param)
%% AWPT_analysis
%       Calculates the (A)WPT of the signal.
%       Filters and decomposition structure are defined by 
%       "fil" and "split_param", respectively
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           04.12.2019
%   last Revision:  13.05.2020
%
%   Input:  signal    ...  (N-D matrix) signal
%           fil       ...  (1 x dim)(1x4) (cell array) filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%           split_param ... struct with parameters which stages and
%                           subbands to use
%
%   Output: coef    ...    (N x (4+2^N) cell matrix) coefficients of the WPT
%                           {
%                           [stage, subband],
%                           support frequencies [fx,...fz],
%                           dual tree description
%                              [Re   Re  ..  Im
%                               ..   ..      ..
%                               Re   Im  ..  Im]
%                           Tree 1    (Re...Re),
%                           Tree 2    (Re...ReIm),
%                           ...
%                           Treee 2^N (Im...Im),
%                           metaData
%                           }
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% condition filter and check for errors

% get number of dimensions
nDim = size(split_param.coef_indizes{1},1);
maxDepth_param = split_param.maxDepth;

% set relevant list indizes to default (every element)
split_param.ind_reduced_list = 1:length(split_param.coef_indizes);

% check if filters for multiple dimensions are specified
filterMultiDim = iscell(fil{1}{1}{1});
if ~filterMultiDim, fil = {fil};end

% count number of specified filter
nFilters = length(fil);
if nFilters>nDim
    warning('AWPT:Analysis:Filters','Too many filters specified. I am going to ignore the additional filters.');
    fil = fil(1:nDim);
elseif nFilters<nDim
    warning('AWPT:Analysis:Filters','Not enough filters specified. I am going to replicate the last given filter to get the necessary number of filters.');
    fil = [fil, repmat(fil(nFilters),1,nDim-nFilters)];
end
    
% get filters maximal lengths
fil_lengths = cellfun(@(x) max([x{3}{1}{3},x{3}{2}{3},x{4}{1}{3},x{4}{2}{3}]), fil);


%% condition input signal type
if iscell(signal)
    for n=1:length(signal)
        % get signal size in relevant dimensions
        signal_dims = size(signal{n});
        signal_dims = signal_dims(1:nDim);
        
        % get possible maximum depth
        maxDepth_fil = ceil(log2(signal_dims./fil_lengths));
        maxDepth = min([maxDepth_fil;maxDepth_param],[],1);
        
        % zeropadding
        if any((2.^maxDepth).*ceil(signal_dims./(2.^maxDepth)) > signal_dims)
            warning('AWPT:Analysis:Samples',['Samples [',num2str(signal_dims),'] are not multiples of [',num2str(2.^maxDepth),']. Signal was zeropadded to [',num2str((2.^maxDepth).*ceil(signal_dims./(2.^maxDepth))),']'])
            signal{n} = padarray(signal{n},(2.^maxDepth).*ceil(signal_dims/(2.^maxDepth))-signal_dims,'post');
        end
    end
    
    sig = signal;
else
    % get signal size in relevant dimensions
    signal_dims = size(signal);
    signal_dims = signal_dims(1:nDim);
    
    % get possible maximum depth
    maxDepth_fil = ceil(log2(signal_dims./fil_lengths));
    maxDepth = min([maxDepth_fil;maxDepth_param],[],1);
    
    % zeropadding
    if any((2.^maxDepth).*ceil(signal_dims./(2.^maxDepth)) > signal_dims)
        warning('AWPT:Analysis:Samples',['Samples [',num2str(signal_dims),'] are not multiples of [',num2str(2.^maxDepth),']. Signal was zeropadded to [',num2str((2.^maxDepth).*ceil(signal_dims./(2.^maxDepth))),']'])
        signal = padarray(signal,(2.^maxDepth).*ceil(signal_dims./(2.^maxDepth))-signal_dims,'post');
    end
    sig = {signal};
end


%% Analytische WPT
[coef] = recursion_start_analysis(sig, fil, split_param);

% If input is not cell unpack Output
if ~iscell(signal)
    coef = coef{1};
end

end
