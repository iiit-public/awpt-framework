% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig_reconstructed = wavelet_packet_synthesis(coeffs, fil)
%% wavelet_packet_synthesis:  calculates the reconstruction of the signals
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           06.11.2019
%   last Revision:  11.12.2019 (Daniel Schwaer)
%
%   Input:  coeffs    ...  (1 x nBatches) cell array of (nCoef x 3) cell
%                           array
%           fil       ...  (1x4) (cell array) filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%
%   Output: sig_reconstructed  ...  (1 x nBatches) signals (nSignales x N)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% get coef structure
if iscell(coeffs{1,1})
    nBatches = size(coeffs,2);
    calcInBatches = true;
else
    nBatches = 1;
    coeffs = {coeffs};
    calcInBatches = false;
end

%% read parameters
coef_list = coeffs{1}(:,2);

%% clean redundant coefficients
% preallocate memory
coeffs_base = cell(1,nBatches);
for idx=1:nBatches
    coeffs_base{idx}= cell(0,size(coeffs{idx},2));
end

% check all coefficients
for n=1:length(coef_list)
    
    % check if coefficient has parent in the list
    has_parent = get_has_parent(coeffs{1}{n,2},coef_list);
    
    % if coefficient has no parent, keep it
    if ~has_parent
        % move all batches to new cell array
        for idx=1:nBatches
            coeffs_base{idx}(end+1,:) = coeffs{idx}(n,:);
        end
    end
end


%% Analytische WPT
sig_reconstructed = cell(1,nBatches);
for n=1:nBatches
    sig_reconstructed{n} = first_stage_synthesis(coeffs_base{n}, fil);
end

% unpack data if input was not packed
if ~calcInBatches
    sig_reconstructed = sig_reconstructed{1};
end

end


