% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [has_child] = get_has_child(current_index, indizes_list)
%% get_has_child: search through the list of indizes if a current index has a child in the wavelet tree
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           12.10.2019
%   last Revision:  23.11.2019
%
%   Input:  current_index   ...     [int,int] 
%                                   current_index(1): stage >=1
%                                   current_index(2): subband \in [0,2^stage-1]
%           indizes_list    ...     (M x 1) cell array with [int,int] with
%                                   all indizes of the tree
%
%   Output: has_child       ...  [bool] returns true if current_index has a
%                                child in the specified tree
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set default
has_child=false;
for n = 1:length(indizes_list)
    temp_ind = indizes_list{n};
    
    % exclude the parent index
    if current_index(1) < temp_ind(1)
        
        % trace back the subband
        trace_back = floor( temp_ind(2)/(2^(temp_ind(1)-current_index(1))) );
        
        % if trace back at current stage is equal to parent index
        if trace_back == current_index(2)
            has_child=true;
            return
        end
        
    end
end


end