% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function save_knot(coef, cost, nBatches, stage, subband, createCostTree)
%% save_knot:  saves the coefficients and costs 
%   
%   Author:         Daniel Schwaer
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           10.12.2019
%   last Revision:  13.12.2019
%
%   Input:  coef            ...  (struct) coef.re{n}(M x N/2)  real part of coeficients
%                                         coef.im{n}(M x N/2)  imaginary part of coefficients
%           cost            ...  (cell array) costs of the positv and negativ frequencys
%           nBatches        ...  (num) number of Batches
%           stage           ...  (num) stage
%           subband         ...  (num) subband
%           creatCostTree   ...  [int >=1] if True -> only costs will be saved
%
%   Output: None
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%global result
global result

% save results for all batches
for idx=1:nBatches
    
    if createCostTree
        result{idx}(end+1,:) = {[],[stage,subband],cost{1},[],cost{2}};
    else
        if ~any(cellfun(@isreal, coef.re)) || ~any(cellfun(@isreal, coef.im))
            result{idx}(end+1,:) = {1/sqrt(2)*(coef.re{idx}+1j*coef.im{idx}) ,...
                [stage,subband],...
                cost{1},...
                1/sqrt(2)*(coef.re{idx}-1j*coef.im{idx}),...
                cost{2}};
        else
            result{idx}(end+1,:) = {1/sqrt(2)*(coef.re{idx}+1j*coef.im{idx}) ,...
                [stage,subband],...
                cost{1}};
        end
    end
end
end