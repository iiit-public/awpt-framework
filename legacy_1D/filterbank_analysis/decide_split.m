% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [split,save_split,cost_low,cost_high] = decide_split(filter, sigLength, split_param, lowCoef, highCoef,stage,subband)
%% decide_split:  calculate the costs of the current coefficients and decide if the tree should be further split
%                 if a preset tree is used, follow this tree, else perform
%                 an automatic forward decision algorithm 
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           12.10.2019
%   last Revision:  22.11.2019
%
%   Input:  filter      ...  (cell array) Filterkoeffizienen
%           sigLength   ...  (num) signal length in input signals
%           split_param ...  struct with parameters which stages and
%                            subbands to use
%           lowCoef     ...  lowCoef.re{n}(M x N/2)  real part of lowpass
%                            lowCoef.im{n}(M x N/2)  imaginary part of lowpass
%           highCoef     ... lowCoef.re{n}(M x N/2)  real part of bandpass
%                            lowCoef.im{n}(M x N/2)  imaginary part of bandpass
%           stage       ...  [int >=1] number of stage 
%           subband     ...  [int] \in [0,2^stage-1] frequency subband in
%                            the current stage
%
%   Output: split     ...  (bool, bool) entscheidung ob weiter aufgespaltet werden soll
%
%   stops if the resulting coefficients would be shorter than the filter
%   length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initalisieren
max_elen_lp = max([filter{1}{1}{3},filter{1}{2}{3}]);
max_elen_bp = max([filter{2}{1}{3},filter{2}{2}{3}]);

split = [0, 0];
save_split = [0,0];
dsf = [1,1];
coefLength = sigLength/2;

% calculate coef costs (lower cost is better, can be negative)
[cost_low{1} , cost_low{2} ] = split_param.hCostFun(lowCoef ,split_param.CostParameters);
[cost_high{1}, cost_high{2}] = split_param.hCostFun(highCoef,split_param.CostParameters);

if strcmp(split_param.Mode,'automatic')
    
    th = split_param.stopping_threshold;
    dsf(1) = mean([cost_low{:}]) < th;
    dsf(2) = mean([cost_high{:}]) < th;
    
    % stopping exception if resulting signal would have less
    % elements than the filters or the maximal depth is reached
    % Low signal
    if (max_elen_lp <= coefLength) && (stage+1<split_param.maxDepth)
        split(1) = dsf(1);
    end
    % high signal
    if (max_elen_bp <= coefLength) && (stage+1<split_param.maxDepth)
        split(2) = dsf(2);
    end
else
    coef_indizes = split_param.coef_indizes;
    
    % check if a child exist
    [split(1)] = get_has_child([stage+1,2*subband],coef_indizes);
    [split(2)] = get_has_child([stage+1,2*subband+1],coef_indizes);
    
    % check if the indizes are contained in the list
    save_split(1) = is_in_coef_list([stage+1,2*subband],coef_indizes);
    save_split(2) = is_in_coef_list([stage+1,2*subband+1],coef_indizes);
    
    % stopping exception resulting signal is to short
    if (max_elen_lp > coefLength)
        split(1) = 0;
        [~, warnId] = lastwarn;
        if ~isequal(warnId,'Split:SignalShort')
            warning('Split:SignalShort',['Signal is too short. Stopping the lowpass splitting at [stage,subband]=[',num2str(stage+1),',',num2str(2*subband),'].']);
        end
    end
    if (max_elen_bp > coefLength)
        split(2) = 0;
        [~, warnId] = lastwarn;
        if ~isequal(warnId,'Split:SignalShort')
            warning('Split:SignalShort',['Signal is too short. Stopping the bandpass splitting at [stage,subband]=[',num2str(stage+1),',',num2str(2*subband+1),'].']);
        end
    end
    
end
 
 
end

