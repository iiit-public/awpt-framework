% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function coef = coefficient_compression(coef,K,sort_basis)
%% coefficient_compression:
%  Sorts the coefficients and removes the smallest coefficients to reach the
%  specified Compression Ratio. The absolute value is used as a default for
%  the sorting algorithm.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           22.11.2019
%   last Revision:  13.12.2019
%
% Input:
%       coef    ...     structure resulting from
%                       wavelet_packet_decomposition
%       K       ...     Compression Ratio
%       sort_basis ...  [optional]: 'abs', 'real', 'imag'
%                       specifies on which basis the coefficients should be
%                       sorted
%
% Output:
%       coef_out ...    same structure as coef
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set defaul values
if nargin < 3
    sort_basis='abs';
end

% check if input is structured in batches
if iscell(coef{1,1})
    nBatches = size(coef,2);
    calcInBatches = true;
else
    nBatches = 1;
    coef = {coef};
    calcInBatches = false;
end


for n=1:nBatches
    % unpack signal
    coef_temp = coef{n};
    % concatenate coefficients
    [~,nCoefs] = cellfun(@size,coef_temp(:,1));
    coef_concatenated_pos = cell2mat(coef_temp(:,1).');
    if size(coef{n},2)==5
        coef_concatenated_neg = cell2mat(coef_temp(:,4).');
    end
    % read signal length and number of signals
    [nSignals,N] = size(coef_concatenated_pos);
    
    % create sorting order by ascending absolute value
    if size(coef{n},2)==5
        switch sort_basis
            case 'abs'
                [~,ColSub] = sort(abs(coef_concatenated_pos)...
                    +abs(coef_concatenated_neg),2);
            case 'real'
                [~,ColSub] = sort(abs(real(coef_concatenated_pos))...
                    +abs(real(coef_concatenated_neg)),2);
            case 'imag'
                [~,ColSub] = sort(abs(imag(coef_concatenated_pos))...
                    +abs(imag(coef_concatenated_neg)),2);
        end
    else
        switch sort_basis
            case 'abs'
                [~,ColSub] = sort(abs(coef_concatenated_pos),2);
            case 'real'
                [~,ColSub] = sort(abs(real(coef_concatenated_pos)),2);
            case 'imag'
                [~,ColSub] = sort(abs(imag(coef_concatenated_pos)),2);
        end
    end
    [~,revColSub] = sort(ColSub,2);

    % create sorting indizes
    RowSub = ((1:nSignals).').*ones(nSignals,N);
    LinIndex = sub2ind([nSignals,N],RowSub,ColSub);
    revLinIndex = sub2ind([nSignals,N],RowSub,revColSub);
    
    %% sort data -> cut of smallest elements -> sort back
    % sort
    coef_compressed_pos = coef_concatenated_pos(LinIndex); 
    
    % cut
    coef_compressed_pos(:,1:(N-round(N/K))) = 0;
    
    % sort back
    coef_compressed_pos = coef_compressed_pos(revLinIndex);
    
    % create cell structure once again
    coef_compressed_pos = mat2cell(coef_compressed_pos,nSignals,nCoefs.').';
    
    
    % pack signal
    coef{n}(:,1) = coef_compressed_pos;
    
    
    if size(coef{n},2)==5
        
        %% sort data -> cut of smallest elements -> sort back
        % sort
        coef_compressed_neg = coef_concatenated_neg(LinIndex);
        % cut
        coef_compressed_neg(:,1:(N-round(N/K))) = 0;
        % sort back
        coef_compressed_neg = coef_compressed_neg(revLinIndex); 
        % create cell structure once again
        coef_compressed_neg = mat2cell(coef_compressed_neg,nSignals,nCoefs.').';
        % pack signal
        coef{n}(:,4) = coef_compressed_neg;
    end
    
end

% create output structure depending on the input structure
if ~calcInBatches
    coef = coef{1};
end

end