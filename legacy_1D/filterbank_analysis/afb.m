% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [sig_lp, sig_bp] = afb(x, fil)
%% SFB  phase adapted 2 channel analyse filter bank based on estimated filter length
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  18.12.2019 (Matthias Baechle)
%
%   Input:  x     ...  input signal [1xN]
%           fil   ...  filter coefficients 1x2 cell 
%
%   Output: sig_lp     ...  low subband output signal [1 x N/2]
%           sig_bp     ...  high subband output signal [1 x N/2]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract filters
f0b = fil{1}{1};
f0a = fil{1}{2};
EL0 = fil{1}{3};
EM0 = fil{1}{4};

f1b = fil{2}{1};
f1a = fil{2}{2};
EL1 = fil{2}{3};
EM1 = fil{2}{4};

% condition effective middle points
M = 2;
EM0 = M*round(EM0/M);
EM1 = M*round(EM1/M);

%% distinguish FIR <-> IIR
if f0a==1
    % use FIR method
    
    % circshift to get symmetric energy distribution around basis function
    [Px,Lx] = size(x);
    x1 = circshift(x, -EM0,2);
    x2 = circshift(x, -EM1,2);

    % zeropadding, preparation for N-D circular convolution
    x1 = [x1, zeros(Px, EL0)];
    x2 = [x2, zeros(Px, EL1)];
    
    % filter lowpass signal
    temp = filter(f0b,f0a,x1,[],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-Lx)=temp(:,1:Lt-Lx)+temp(:,Lx+1:end);
    sig_lp = temp(:,1:2:Lx);
    
    % filter bandpass signal
    temp = filter(f1b,f1a,x2,[],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-Lx)=temp(:,1:Lt-Lx)+temp(:,Lx+1:end);
    sig_bp = temp(:,1:2:Lx);
else
    % use IIR method
    
    % circshift to get symmetric energy distribution around basis function
    MEM=round((EM0+EM1)/2);
    [Px,Lx] = size(x);
    x1= circshift(x, -EM0,2);
    x2 = circshift(x, -EM1,2);
    % zeropadding, preparation for N-D circular convolution
    x1 = [x1, zeros(Px, EL0+MEM)];
    x2 = [x2, zeros(Px, EL1+MEM)];
    
    % filter lowpass signal
    temp = filter(f0b,f0a,x1,[],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-Lx)=temp(:,1:Lt-Lx)+temp(:,Lx+1:end);
    sig_lp = temp(:,1:2:Lx);
    
    % filter bandpass signal
    temp = filter(f1b,f1a,x2,[],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-Lx)=temp(:,1:Lt-Lx)+temp(:,Lx+1:end);
    sig_bp = temp(:,1:2:Lx);
end







