% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [base,base_FT,f] = calc_base_function_1D(s_fil,stage,subband,time_step,N,fs)
%% calc_base_function_1D
%       calculates the base function corresponding to the 
%       specified coefficient of the AWP
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           11.11.2019
%   last Revision:  20.02.2020
%
%   Input:  s_fil      ...  (1x4) (cell array) synthesis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           stage      ...  [int] scale in the wavelet tree >0
%           subband    ...  [int] in [0,2^stage-1] specifies branch
%           time_step  ...  [int] time step of the coeficient in the
%                           subband
%           N          ...  number of samples of base function
%           fs         ...  sampling frequency
%
%   Output: base       ...  resulting (1 x N) of the wavelet synthesis
%                           filterbank
%           base_FT    ...  fft with fftshif of base (1 x N) complex
%           f          ...  -fs/2 .. fs/2 with df = fs/N
%                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% condition signal length 
% calculate minimum allowed coefficient length
nCoeffs_min = max([s_fil{1}{1}{3},...
                s_fil{1}{2}{3},...
                s_fil{2}{1}{3},...
                s_fil{2}{2}{3}...
                ]);
            
% calculate minimum necessary signal length            
N_min = max([2^ceil(log2(nCoeffs_min)),2^ceil(log2(time_step+1))]);

% set N to a multiple of 2^stage
if N < N_min
    N=N_min;
else
    N = 2^ceil(log2(N));
end

% calculate coefficient length
nCoefs = N*2^(-stage);

%% create coefficient structure
% create coefficient vector
coef_vec = zeros(1,nCoefs);
coef_dummy = zeros(1,nCoefs);
coef_vec(time_step+1) = 1;

% check if analytical or real wavelet packets are used
if isequal(s_fil{1}{1},s_fil{1}{2})
    %% real wavelet packets
    % fill structure with coefficient of interest
    coef = {{coef_vec/2,[stage,subband],[],coef_vec/2,[]}};
else
    %% analytic wavelet packets 
    % fill structure with coefficient of interest
    coef = {{coef_vec,[stage,subband],[],coef_dummy,[]}};
end


% fill the rest with zeros
for k = 0:2^stage-1
    if k~=subband
        coef{1}(end+1,:) = {coef_dummy,[stage,k],[],coef_dummy,[]}; 
    end
end

%% reconstruct signal from coefficient structure
base  = wavelet_packet_synthesis(coef, s_fil);

%% condition output parameters abnd perform Fourier transform
base = base{1};

% distinguish analytical and real wavelet packets
if ~isequal(s_fil{1}{1},s_fil{1}{2})
    % analytical wavelet packets
    base = hilbert(0.5*base);
    base_FT = fft(base);
else
    % real wavelet packets
    base_FT = fft(real(base));
end
df = fs/N;
f = (-N/2:N/2-1)*df;
base_FT = fftshift(base_FT);


end
