% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_Compression.m:  Test the function wavelet_packet_signal_compression.
%                       Uses a chirp as input signal and displays the sum
%                       of squared errors and the compressed signal
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           25.11.2019
%   last Revision:  24.02.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% prepare workspace
close all;
clear;
clc;

%% Defines
nSignals = 1;
N = 8192;
Fs = 50e6;
Ts = 1/Fs;
t = 0:Ts:(N-1)*Ts;

% define test signal
signal = cos(2*pi*(700e3*ones(nSignals,1) + 20e6/(2*8192*2e-8)*t).*t) - 1i*sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
signal = signal./sqrt(signal*signal');


%% define test cases
for test_case = 1:6
    switch test_case
        case 1 % basis test
            [a_fil,s_fil] = get_filters('db',10,1,0,'hwlet',20,1,8, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 2 ,a_fil,s_fil);
        case 2 % compression factor test
            [a_fil,s_fil] = get_filters('db',10,1,0,'hwlet',20,1,8, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 5.5 ,a_fil,s_fil);
        case 3 % filter symlet test
            [a_fil,s_fil] = get_filters('sym',10,1,0,'hwlet',20,1,8, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 5.5 ,a_fil,s_fil);
        case 4 % fractional delay test
            [a_fil,s_fil] = get_filters('db',10,1,0,'hwlet',20,1,6, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 5.5 ,a_fil,s_fil);
        case 5 % vanishing moment test
            [a_fil,s_fil] = get_filters('db',10,1,0,'hwlet',20,2,6, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 5.5 ,a_fil,s_fil);
        case 6 % first stage filter test
            [a_fil,s_fil] = get_filters('db',20,1,0,'hwlet',20,1,8, 'min_phase');
            [signal_decompressed, coef] = wavelet_packet_signal_compression(signal, 5.5 ,a_fil,s_fil);
    end
    disp(['Test case ',num2str(test_case),': SSE = ',...
        num2str((signal_decompressed-signal)*(signal_decompressed-signal)')...
        ])
end

%% decompression figure of the last test case
figure;
subplot(2,1,1);
plot(real(signal(1,:)));hold on;plot(real(signal_decompressed(1,:)),'.')
subplot(2,1,2);
plot(imag(signal(1,:)));hold on;plot(imag(signal_decompressed(1,:)),'.')


