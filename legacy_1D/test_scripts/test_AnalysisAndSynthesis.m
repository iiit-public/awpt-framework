% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_AnalysisAndSynthesis.m:  testing different tree structures
%   
%   Author:         Daniel Schwaer, Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.11.2019
%   last Revision:  24.02.2020
%
% This function is the test script for the function set_split_parameters
% wavelet_packet_decomposition and wavelet_packet_synthesis
% The results are representated as error of the signal reconstruction
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear;
clc;

%% Defines
nSignals = 10;
N = 8192;
Fs = 50e6;
Ts = 1/Fs;
t = 0:Ts:(N-1)*Ts;

yH_meas = sin(2*pi*(700e3*ones(nSignals,1) + 20e6/(2*8192*2e-8)*t).*t);
yR_meas = sin(2*pi*(700e3*ones(nSignals,1) + 20e6/(2*8192*2e-8)*t).*t);

yH_meas = yH_meas./sqrt(sum(yH_meas.^2,2));
yR_meas = yR_meas./sqrt(sum(yR_meas.^2,2));

%% split parameter
th = -0.01*sum(2*yH_meas(1,:).^2);
% test cases
for test_case =1:12
    
    % filter design
    [a_fil,s_fil] = get_filters('db',20,1,0,'hwlet',20,1,8, 'min_phase');
    
    switch test_case
        case 1
            % should create no coefficents set
            split_param = set_split_parameters([], 'preset_tree');
        case 2
            split_param = set_split_parameters({[2,0],[2,1],[3,2],[3,3],[1,1]});
        case 3
            % should create no maximum depth set
            split_param = set_split_parameters([],'wavelet_tree');
        case 4
            split_param = set_split_parameters([],'wavelet_tree',3);
        case 5
            % should create no maximum depth set
            split_param = set_split_parameters([],'full_tree');
        case 6
            split_param = set_split_parameters([],'full_tree',7);
        case 7
            % should create not enough parameters warning
            split_param = set_split_parameters([],'automatic');
        case 8
            % should create not enough parameters warning
            split_param = set_split_parameters([],'automatic',3);
        case 9
            split_param = set_split_parameters([],'automatic',6,th);
        case 10
            split_param = set_split_parameters({[2,0],[2,1],[3,2],[3,3],[1,1]},'preset_tree',6,th);
        case 11
            split_param = set_split_parameters([],'automatic',10,th,@hCostEnergy,[]);
        case 12
            % with too many stages: possible to create signal too short warning 
            split_param = set_split_parameters([],'full_frame',9,[],@hCostEnergy,[],true);
            [coef_cost_tree] = wavelet_packet_decomposition({yH_meas,yR_meas}, a_fil,split_param);
            split_param = set_split_parameters([],'full_tree',9,[],@hCostEnergy,[],false);
    end
    
    % decomposition
    [coef] = wavelet_packet_decomposition({yH_meas,yR_meas}, a_fil,split_param);
end


%% reconstruction
signals_reconstructed  = wavelet_packet_synthesis(coef, s_fil);


%% show results
yH_meas_recon = signals_reconstructed{1};
yR_meas_recon = signals_reconstructed{2};


reconstuction_error = sum(abs(yH_meas(1,:)-yH_meas_recon(1,1:N)).^2,'all')


figure;
plot(yH_meas(1,:));hold on;plot(yH_meas_recon(1,:),'.')
coef_yH = coef{1};
[matrix,f,t] = plot_WPT(coef_yH,50e6,'surface');



%% local cost functions
function [cost_pos,cost_neg] = hCostEntropy(coefs,parameters)
    nBatches = length(coefs.re);
    batch_cost_pos = zeros(1,nBatches);
    batch_cost_neg = zeros(1,nBatches);
    for idx=1:nBatches
        % positive frequency
        coefs_abs = abs(coefs.re{idx} + 1i*coefs.im{idx});
        coefs_abs(isnan(coefs_abs)) = [];
        coefs_abs(coefs_abs==0) = [];
        batch_cost_pos(idx) = -sum((coefs_abs.^2).*log((coefs_abs.^2)));
        
        % negative frequency
        coefs_abs = abs(coefs.re{idx} - 1i*coefs.im{idx});
        coefs_abs(isnan(coefs_abs)) = [];
        coefs_abs(coefs_abs==0) = [];
        batch_cost_neg(idx) = -sum((coefs_abs.^2).*log((coefs_abs.^2)));
    end
    cost_pos = mean(batch_cost_pos);
    cost_neg = mean(batch_cost_neg);
end

function [cost_pos,cost_neg] = hCostEnergy(coefs,parameters)
    nBatches = length(coefs.re);
    batch_cost_pos = zeros(1,nBatches);
    batch_cost_neg = zeros(1,nBatches);
    for idx=1:nBatches
        batch_cost_pos(idx) = -mean(sum(abs(coefs.re{idx} + 1i*coefs.im{idx}).^2,2),1);
        batch_cost_neg(idx) = -mean(sum(abs(coefs.re{idx} - 1i*coefs.im{idx}).^2,2),1);
    end
    cost_pos = mean(batch_cost_pos);
    cost_neg = mean(batch_cost_neg);
end

