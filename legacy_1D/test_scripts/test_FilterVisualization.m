% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_FilterVisualization.m:  
%       In this script, the two methods to visualize the base functions and
%       their spectrum are testet. Every base function is completely
%       described using the stage, subband, and time step and 
%       most importantly the filter transfer functions.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           26.11.2019
%   last Revision:  20.02.2020
%
%   The two methods "calc_base_function.m" and "calc_base_response.m" are tested. 
%   In "calc_base_response" the wavelet filterbank is excited using
%   harmonic signals and the response of the specified coefficient is
%   measured. Therefore, this function ("calc_base_response") only allows 
%   a spectral information and spectral leakage is a problem. To reduce
%   spectral leakage use frequency bins, that fit into the time window N*dt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% prepare workspace 
clear;
close all;
clc;

%% test real wavelet packets

% set test parameters
stages = 3:4;
subbands = 2:5;
fs = 50e6;
N = 1024;
time_step = 20;

% filter design real wavelets
[a_fil,s_fil] = set_filters('db',20,1,0);

% create figure
hfig = figure;
hfig.WindowState = 'maximized';
subplot(length(subbands),length(stages),1);


% perform test for different stages and subbands
for n=1:length(stages)
    stage = stages(n);
    for k = 1:length(subbands)
        subband = subbands(k);
        
        % calculate base functions in spectral domain
        [base,base_FT,f] = calc_base_function_1D(s_fil,stage,subband,time_step,N,fs);
        [coef,f_vec] = calc_base_response_1D(a_fil,stage,subband,time_step,N,f,fs);
        
        % plot base function in spectral domain
        subplot(length(subbands),length(stages),(k-1)*length(stages) + n);
        plot(f,abs(base_FT),f,abs(coef),'.');
        title(['Stage ',num2str(stage),', subband ',num2str(subband)]);
        xlabel('f/kHz');
        ylabel('Spectral density');
        legend('Base signal Fourier transformed','Base response of coefficient');
    end
end


%% test analytical wavelet packets

% set test parameters
stages = 3:4;
subbands = 2:5;
fs = 50e6;
N = 1024;
time_step = 20;

% filter design
[a_fil,s_fil] = set_filters('db',20,1,0,'hwlet',20,1,8, 'min_phase');

% create figure
hfig2 = figure;
hfig2.WindowState = 'maximized';
subplot(length(subbands),length(stages),1);


% perform test for different stages and subbands
for n=1:length(stages)
    stage = stages(n);
    for k = 1:length(subbands)
        subband = subbands(k);
        
        % calculate base functions in spectral domain
        [base,base_FT,f] = calc_base_function_1D(s_fil,stage,subband,time_step,N,fs);
        [coef,f_vec] = calc_base_response_1D(a_fil,stage,subband,time_step,N,f,fs);
        
        % plot base function in spectral domain
        subplot(length(subbands),length(stages),(k-1)*length(stages) + n);
        plot(f,abs(base_FT),f,abs(coef),'.');
        title(['Stage ',num2str(stage),', subband ',num2str(subband)]);
        xlabel('f/kHz');
        ylabel('Spectral density');
        legend('Base signal Fourier transformed','Base response of coefficient');
    end
end

