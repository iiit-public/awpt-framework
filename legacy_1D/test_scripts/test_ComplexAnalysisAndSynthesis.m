% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_ComplexAnalysisAndSynthesis.m:  
% Testing the reconstruction of the wavelet packet, if real or complex 
% input signals are used.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           29.11.2019
%   last Revision:  24.02.2020
%
% This function is the test script for the function "wavelet_packet_decomposition"
% and "wavelet_packet_synthesis".
% The results are representated as error of the signal reconstruction
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear;
clc;

%% Defines
nSignals = 10;
N = 8192;
Fs = 50e6;
Ts = 1/Fs;
t = 0:Ts:(N-1)*Ts;

% filter design
[a_fil,s_fil] = get_filters('db',20,1,0,'hwlet',20,1,8, 'min_phase');

% set split specifications
split_param = set_split_parameters([], 'full_tree',4);

% def cases
cases_description = {'Analytical Wavelet Packets, Complex Signal positiv Frequency',...
                     'Analytical Wavelet Packets, Complex Signal negativ Frequency',...
                     'Analytical Wavelet Packets, Real Signal',...
                     'Real Wavelet Packets, Real Signal',...
                     'Real Wavelet Packets, Complex Signal positive Frequency',...
                     'Real Wavelet Packets, Complex Signal negative Frequency'};

disp('Reconstruction Error:')
                 
% test cases
for test_case =1:6
    
    switch test_case
        case 1
            % create signal complex signal with positiv frequency
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t)...
                + 1i*sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
            
        case 2
            % create complex signal with negativ frequency
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t)...
                - 1i*sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
            
        case 3
            % create real signal
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
            
        case 4
            % create real signal
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % set real wavelet packets
            [a_fil,s_fil] = get_filters('db',20,1,0);
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
            
        case 5
            % create complex signal
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t)...
                + 1i*sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % set real wavelet packets
            [a_fil,s_fil] = get_filters('db',20,1,0);
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
            
        case 6
            % create complex signal
            sig = cos(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t)...
                - 1i*sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
            sig = sig./sqrt(sig*sig');
            
            % set real wavelet packets
            [a_fil,s_fil] = get_filters('db',20,1,0);
            
            % perform decomposition
            [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
            % perform reconstruction
            sig_rec  = wavelet_packet_synthesis(coefs, s_fil);
            
            % reconstruction error and plot
            error = (sig - sig_rec)*(sig - sig_rec)' / (sig*sig') *100;
            
            % plot results
            figure('Position',[330,140,1170,780]);
            subplot(2,1,1);
            plot(t,real(sig),t,real(sig_rec),'.')
            title(['Real Part, ',cases_description{test_case}]);
            
            subplot(2,1,2);
            plot(t,imag(sig),t,imag(sig_rec),'.')
            title(['Imaginary Part, ',cases_description{test_case}]);
    end
    
    disp([cases_description{test_case},' = ', num2str(error),'%']);
    
end


