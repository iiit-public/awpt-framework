% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_Filter.m:  testing different filter designs
%   
%   Author:         Daniel Schwaer, Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.11.2019
%   last Revision:  24.02.2020
%
% This function is the test script for the function get_example_filter and
% different filter combinations
% The results are representated as time-frequency-representation for the
% example filters and as reconstruction errors for the rest of the filters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear;
clc;

%% Defines
nSignals = 2;
N = 8192;
fs = 50e6;
Ts = 1/fs;
t = 0:Ts:(N-1)*Ts;

signal = sin(2*pi*(700e3*ones(nSignals,1) + 20e6/(2*8192*2e-8)*t).*t);
signal = signal./sqrt(sum(signal.^2,2));


%% test cases for example filters
for filter =1:6
    % filter design
    [an_fil, syn_fil] = get_example_filter(filter);
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    plot_WPT(coef, fs, 'surface', 1);
end

%% test Daubechies filters
for n=1:10
    
    [an_fil,syn_fil] = get_filters('db',n,1,0);
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%']);
end

%% test Fejer-Korovkin filters
for n=2:4
    
    [an_fil,syn_fil] = get_filters('fk',n*2,1,0);
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end


%% test Symlets
for n=2:6
    
    [an_fil,syn_fil] = get_filters('sym',n,1,0);
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end


%% test Coiflets
for n=1:5
    
    [an_fil,syn_fil] = get_filters('coif',n,1,0);
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         Analytical Wavelet Packets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\nAnalytical Wavelet Packets\n\n')

%% test Daubechies filters
for n=1:10
    
    [an_fil,syn_fil] = get_filters('db',n,1,0,'hwlet',1,1,4,'mid_phase');
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%']);
end

%% test Fejer-Korovkin filters
for n=2:4
    
    [an_fil,syn_fil] = get_filters('fk',n*2,1,0,'hwlet',1,1,4,'mid_phase');
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end


%% test Symlets
for n=2:6
    
    [an_fil,syn_fil] = get_filters('sym',n,1,0,'hwlet',1,1,4,'mid_phase');
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end


%% test Coiflets
for n=1:5
    
    [an_fil,syn_fil] = get_filters('coif',n,1,0,'hwlet',1,1,4,'mid_phase');
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end

%% test Hwlet filters
for n=1:4
    for k=1:4
        
        [an_fil,syn_fil] = get_filters('db',2,1,0,'hwlet',1,n,k,'mid_phase');
        
        % decomposition
        split_param = set_split_parameters([], 'full_tree', 6);
        coef = wavelet_packet_decomposition(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = wavelet_packet_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        disp(['Reconstruction Error (Hwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%']);
    end
end

%% test Bwlet filters
for n=2:5
    for k=2:5
        
        [an_fil,syn_fil] = get_filters('db',2,1,0,'bwlet',1,n,k,'lin_phase');
        
        % decomposition
        split_param = set_split_parameters([], 'full_tree', 6);
        coef = wavelet_packet_decomposition(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = wavelet_packet_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        disp(['Reconstruction Error (Bwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%']);
    end
end

%% test Qshift filters
for n=4:2:14
    
    [an_fil,syn_fil] = get_filters('db',2,1,0,'qshift',n,1,1,'mid_phase');
    
    % decomposition
    split_param = set_split_parameters([], 'full_tree', 6);
    coef = wavelet_packet_decomposition(signal, an_fil,split_param);
    
    % reconstruction
    signal_rec = wavelet_packet_synthesis(coef,syn_fil);
    
    ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
    disp(['Reconstruction Error (Qshift ',num2str(n),'): ',num2str(ReconstructionError),'%']);
end
