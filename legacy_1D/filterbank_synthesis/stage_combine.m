% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function result = stage_combine(lp_coef_re,bp_coef_re,fil, mem)
%% stage_combine:  combine the two bands to get the next stage, 
%                  based on only the real tree
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  17.12.2019 (Daniel Schwaer)
%
%   Input:  lp_coef     ...  low frequency signals [nSignals x N]
%           hp_coef     ...  high frequency signals [nSignals x N]
%           fil         ...  (1 x 2 cell array) filter coefficients
%           mem         ...   change the order of low- and high-band according to
%                             weickert [integer]
%                             1: real imag filter without exchange
%                             2: exchange real <-> imag
%                                         lp   <-> hp
%                             3: use only real filter without exchange
%                            -3: use only real filter, exchange lp <-> hp
%
%   Output: result      ...  complex output signal [nSignals x N]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% extract filter pairs
fil_re{1} = fil{1}{1}; % real-filter, lowpass
fil_re{2} = fil{2}{1}; % real-filter, bandpass
fil_im{1} = fil{1}{2}; % imag-filter, lowpass
fil_im{2} = fil{2}{2}; % imag-filter, bandpass

switch mem
    case 1  % BP->BP, LP->LP
        sig_re = sfb(lp_coef_re, bp_coef_re, fil_re);
    case 2  % LP->BP, BP->LP
        sig_re = sfb(bp_coef_re, lp_coef_re, fil_im);
    case 3  % BP->BP, LP->LP
        sig_re = sfb(lp_coef_re, bp_coef_re, fil_re);
    case -3 % LP->BP, BP->LP
        sig_re = sfb(bp_coef_re, lp_coef_re, fil_re);
end

% without imaginary tree
result = sig_re;
end