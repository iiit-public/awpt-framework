% Copyright (C) 2020  The AWP-Framework Authors
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_1D_AWP.m  testing the decomposition and reconstruction of multiple 1D signals using
%                  the multi-dimensional AWPs
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           25.02.2020
%   last Revision:  01.06.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear;
clc;

%% Defines
nSignals = 2;
N = 8192;
Fs = 1;
Ts = 1/Fs;
t = (0:Ts:(N-1)*Ts).';

sigs1 = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
sigs1 = repmat(sigs1,1,nSignals);

sigs2 = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
sigs2 = repmat(sigs2,1,nSignals);

sigs1 = sigs1./sqrt(sum(sigs1.^2,1));
sigs2 = sigs2./sqrt(sum(sigs2.^2,1));


% test cases
for test_case =1:7
    
    % filter design
    [a_fil,s_fil] = get_filters('db',20,1,0,'hwlet',20,1,8, 'minphase');
    
    switch test_case
        case 1
            % should create no coefficents set
            warning('off','AWPT:get_decomposition_structure:MissingIndizes');
            warning('off','AWPT:Plot1D:SignalDimension');
            try
                split_param = get_decomposition_structure([], 'preset_tree');
                % catch warning decomp
                [~,warnId1] = lastwarn;
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'analytic');
                plot_Subbands1D(coef{1},Fs);
                % catch warning plot multi signals
                [~,warnId2] = lastwarn;
                if isequal(warnId1,'AWPT:get_decomposition_structure:MissingIndizes') && isequal(warnId2,'AWPT:Plot1D:SignalDimension')
                    fprintf('Preset tree test with missing indizes: Passed\n');
                else
                    fprintf('Preset tree test with missing indizes: Failed\n');
                end
            catch
                fprintf(2,'Preset tree test with missing indizes: Failed\n');
            end
            warning('on','AWPT:get_decomposition_structure:MissingIndizes');
            warning('on','AWPT:Plot1D:SignalDimension');
        case 2
            sigs1 = sigs1(:,1);
            sigs2 = sigs2(:,1);
            try
                split_param = get_decomposition_structure({[2,0],[2,1],[3,2],[3,3],[1,1]});
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'imag');
                plot_Subbands1D(coef{1},Fs);
                fprintf('Preset tree test: Passed\n');
            catch
                fprintf(2,'Preset tree test: Failed\n');
            end

        case 3
            warning('off','AWPT:get_decomposition_structure:MissingMaxDepth');
            try
                % should create no maximum depth set
                split_param = get_decomposition_structure([],'wavelet_tree');
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'analytic');
                plot_Subbands1D(coef{1},Fs);
                
                % catch warning
                [~,warnId] = lastwarn;
                if isequal(warnId,'AWPT:get_decomposition_structure:MissingMaxDepth')
                    fprintf('Wavelet tree test with missing depth: Passed\n');
                else
                    fprintf(2,'Wavelet tree test with missing depth: Failed\n');
                end
            catch
                fprintf(2,'Wavelet tree test with missing depth: Failed\n');
            end
            warning('on','AWPT:get_decomposition_structure:MissingMaxDepth');
        case 4
            try
                split_param = get_decomposition_structure([],'wavelet_tree',1,6);
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'analytic');
                plot_Subbands1D(coef{1},Fs);
                fprintf('Wavelet tree test: Passed\n');
            catch
                fprintf(2,'Wavelet tree test: Failed\n');
            end
        case 5
            warning('off','AWPT:get_decomposition_structure:MissingMaxDepth');
            try
                % should create no maximum depth set
                split_param = get_decomposition_structure([],'full_tree',1);
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'analytic');
                plot_Subbands1D(coef{1},Fs);
                % catch warning
                [~,warnId] = lastwarn;
                if isequal(warnId,'AWPT:get_decomposition_structure:MissingMaxDepth')
                    fprintf('Full tree test with missing depth: Passed\n');
                else
                    fprintf(2,'Full tree test with missing depth: Failed\n');
                end
            catch
                fprintf(2,'Full tree test with missing depth: Failed\n');
            end
            warning('on','AWPT:get_decomposition_structure:MissingMaxDepth');
        case 6
            try
                split_param = get_decomposition_structure([],'full_tree',1,7);
                % decomposition
                [coef] = AWPT_analysis({sigs1,sigs2}, a_fil,split_param);
                coef = get_analytic_coefficients(coef,'analytic');
                plot_Subbands1D(coef{1},Fs);
                fprintf('Full tree test: Passed\n');
            catch
                fprintf(2,'Full tree test: Failed\n');
            end
        case 7
            warning('off','AWPT:Analysis:Samples')
            try
                % test case of samples number not multiples of 2^depth
                sig_test = sigs1(1:4100);
                split_param = get_decomposition_structure([],'wavelet_tree',1,6);
                % decomposition
                [coef] = AWPT_analysis(sig_test, a_fil,split_param);
                
                % catch warning
                [~,warnId] = lastwarn;
                if isequal(warnId, 'AWPT:Analysis:Samples')
                    fprintf('Test samples are not multiples of 2^depth: Passed\n');
                else
                    fprintf(2,'Test samples are not multiples of 2^depth: Failed\n');
                end
            catch
                fprintf(2,'Test samples are not multiples of 2^depth: Failed\n');
            end
    end
    
end




