% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_1D_Denoising.m:
%       testing the denoising of 1D signals using different threshold
%       estimations, hard- and soft thresholding and different wavelet tree
%       combinations
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           01.07.2020
%   last Revision:  01.07.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear workspace and open figures
close all;
clc;
clear;

% test signal
load fdata.mat

% show noisy image
figure;
subplot(3,3,1);
plot(fNoisy);hold on; plot(fClean);
legend('Noisy','Original');
title('Source Data');

% split parameter
split_param = get_decomposition_structure([],'wavelet_tree',1,10);

% filter design
[a_fil,s_fil] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');

% decomposition
[coef] = AWPT_analysis(fNoisy, a_fil,split_param);

% estimate noise
[a_fil_noise_est,~] = get_filters('db',2,[],[]);
sigma = MAD_noise_estimation(fNoisy,[1,1],a_fil_noise_est);

for test_case = 1:8
    switch test_case
        case 1
            % baseline is matlab denoising method
            sig_denoised = wdenoise(fNoisy,10,'Wavelet','db2');
            
            % calculate PSNR of denoised signal
            PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
            
            % print results
            fprintf('Matlab Baseline, wavelet tree (PSNR = %2.2f)\n',PSNR);
            
            % show results
            subplot(3,3,test_case+1);
            plot(sig_denoised);
            title(['Matlab DWT, Bayesian Cauchy Prior', ' (',num2str(PSNR,'%2.2f'),'dB)']);
        case 2
            try

                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'soft';
                wavelet_mode = 'analytic';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink soft, DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DT-CWT, Visu, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('VisuShrink soft, DT-CWT: Failed\n');
            end
        case 3
            try
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'hard';
                wavelet_mode = 'analytic';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink hard, DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DT-CWT, Visu, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('VisuShrink hard, real DT-CWT: Failed\n');
            end
        case 4
            try
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'soft';
                wavelet_mode = 'analytic';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink soft, DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DT-CWT, Sure, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('SureShrink soft, DT-CWT: Failed\n');
            end
        case 5
            try
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'hard';
                wavelet_mode = 'analytic';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink hard, DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DT-CWT, Sure, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('SureShrink hard, DT-CWT: Failed\n');
            end
        case 6
            try
                % calculate DWT with db2
                [a_fil,s_fil] = get_filters('db',2,[],[]);
                [coef] = AWPT_analysis(fNoisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'hard';
                wavelet_mode = 'single';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink hard, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DWT, Visu, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
            catch
                
                % print error code
                fprintf('VisuShrink hard, DWT: Failed\n');
            end
        
        case 7
            try
                % calculate DWT with db2
                [a_fil,~] = get_filters('db',2,[],[]);
                [coef] = AWPT_analysis(fNoisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'soft';
                wavelet_mode = 'single';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink soft, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DWT, Visu, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
            catch
                
                % print error code
                fprintf('VisuShrink soft, DWT: Failed\n');
            end
            
        case 8
            try
                
                % calculate DWT with db2
                [a_fil,~] = get_filters('db',2,[],[]);
                [coef] = AWPT_analysis(fNoisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'soft';
                wavelet_mode = 'single';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                sig_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(fClean - sig_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink soft, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,3,test_case+1);
                plot(sig_denoised);
                title(['DWT, Sure, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
            catch
                
                % print error code
                fprintf('SureShrink soft, DWT: Failed\n');
            end
        
    end
    
end




