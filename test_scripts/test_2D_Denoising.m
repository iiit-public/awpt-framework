% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_2D_Denoising.m:
%       testing the denoising of images using different threshold
%       estimations, hard- and soft thresholding and different wavelet tree
%       combinations
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           01.06.2020
%   last Revision:  01.07.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear workspace and open figures
close all;
clc;
clear;

% set test parameters
SNR = 20; % in dB
sigma = 10^(-SNR/20);
nTestCases = 10;

% test image
Image = double(imread('cameraman.tif'));
Image = Image./max(Image,[],'all');

% add noise
Image_noisy = Image + sigma*randn(size(Image));

% show noisy image
figure;
subplot(3,4,1);
imshow(Image_noisy);
title(['Noisy Image', ' (',num2str(SNR),'dB)']);

% split parameter
split_param = get_decomposition_structure([],'wavelet_tree',2,[3,3]);

% filter design
% dimension 1
[a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
% dimension 2
[a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');

% decomposition
[coef] = AWPT_analysis(Image_noisy, a_fil,split_param);

for test_case = 1:nTestCases
    switch test_case
        case 1
            % baseline is matlab denoising method
            [Image_denoised,CXC,LXC] = wdencmp('gbl',Image_noisy,'db2',3,sigma*sqrt(2*log(numel(Image_noisy))),'s',1);
            
            % calculate PSNR of denoised signal
            PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
            
            % print results
            fprintf('Matlab Baseline soft, wavelet tree (PSNR = %2.2f)\n',PSNR);
            
            % show results
            subplot(3,4,test_case+1);
            imshow(Image_denoised);
            title(['Matlab DWT, Visu, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
        case 2
            try
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'soft';
                wavelet_mode = 'real';

                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);

                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink soft, real DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['Real DT-CWT, Visu, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('VisuShrink soft, real DT-CWT: Failed\n');
            end
        case 3
            try
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'soft';
                wavelet_mode = 'real';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink soft, real DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['Real DT-CWT, Sure, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('SureShrink soft, real DT-CWT: Failed\n');
            end
        case 4
            try
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'hard';
                wavelet_mode = 'real';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink hard, real DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['Real DT-CWT, Visu, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('VisuShrink hard, real DT-CWT: Failed\n');
            end
        case 5
            try
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'hard';
                wavelet_mode = 'real';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink hard, real DT-CWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['Real DT-CWT, Sure, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
            catch
                % print error code
                fprintf('SureShrink hard, real DT-CWT: Failed\n');
            end
        case 6
            try
                
                % filter design (set decomposition to DWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Visu';
                thres_mode = 'soft';
                wavelet_mode = 'single';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink soft, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['DWT, Visu, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
            catch
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % print error code
                fprintf('VisuShrink soft, DWT: Failed\n');
            end
        case 7
            try
                
                % filter design (set decomposition to DWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'soft';
                wavelet_mode = 'single';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink soft, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['DWT, Sure, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
            catch
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % print error code
                fprintf('SureShrink soft, DWT: Failed\n');
            end
        case 8
            try
                
                % filter design (set decomposition to DWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'hard';
                wavelet_mode = 'single';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink hard, DWT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['DWT, Sure, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
            catch
                
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % print error code
                fprintf('SureShrink hard, DWT: Failed\n');
            end
        case 9
            try
                
                % get rekombination and signal energy parameters
                signal_energy = sum(Image_noisy.^2,'all');
                [~,A] = get_analytic_coefficients(coef,'real');
                
                split_param = get_decomposition_structure([],'full_frame',2,[5,5],@hCostEntropyRealRekombination,{signal_energy,A},true);
                
                % decomposition with optimal basis search
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                coef_list = optimal_basis_search(coef);
                split_param = get_decomposition_structure(coef_list,'preset_tree');
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % set denoising parameters
                thres_estimation = 'Sure';
                thres_mode = 'soft';
                wavelet_mode = 'real';
                
                % perform denoising
                [coef_denoised] = wavelet_denoising(coef,sigma,thres_estimation,thres_mode,wavelet_mode);
                
                % reconstruction
                Image_denoised = AWPT_synthesis(coef_denoised,s_fil);
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('SureShrink soft, Real SA-AWPT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['Real SA-AWPT, Sure, soft', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
                % reset to DWT
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                % tree structure
                split_param = get_decomposition_structure([],'wavelet_tree',2,[3,3]);
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
            catch
                
                % reset to DWT
                % filter design (set decomposition back to DT-CWT)
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',0,2,2,'midphase');
                % tree structure
                split_param = get_decomposition_structure([],'wavelet_tree',2,[3,3]);
                % decomposition
                [coef] = AWPT_analysis(Image_noisy, a_fil,split_param);
                
                % print error code
                fprintf('SureShrink hard, Real SA-AWPT: Failed\n');
            end
        case 10
            try
                
                thres_val = sigma*sqrt(2*log(numel(Image_noisy)));
                
                % Analyse, transform noise light field
                Image_noisy_dct = dct(Image_noisy, [], 1);
                Image_noisy_dct = dct(Image_noisy_dct, [], 2);
                
                thres_mask = abs(Image_noisy_dct) < thres_val;
                
                % Apply threshold
                Image_noisy_dct_thres = Image_noisy_dct;
                Image_noisy_dct_thres(thres_mask) = 0;
                
                % Reconstruction
                Image_denoised = idct(Image_noisy_dct_thres, [], 1);
                Image_denoised = idct(Image_denoised, [], 2);
                
                % clipping
                Image_denoised(Image_denoised > 1) = 1;
                Image_denoised(Image_denoised < 0) = 0;
                
                % calculate PSNR of denoised signal
                PSNR = 10*log10(1/mean((abs(Image - Image_denoised).^2),'all'));
                
                % print results
                fprintf('VisuShrink hard, DCT (PSNR = %2.2f)\n',PSNR);
                
                % show results
                subplot(3,4,test_case+1);
                imshow(Image_denoised);
                title(['DCT, Visu, hard', ' (',num2str(PSNR,'%2.2f'),'dB)']);
                
            catch
                
                % print error code
                fprintf('VisuShrink hard, DCT: Failed\n');
            end
    end
    
end



function [cost] = hCostEntropyRealRekombination(coefs,parameters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% read parameters from input
signal_energy = parameters{1};
A = parameters{2};
nTrees = length(coefs{1});

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);
signalDims = sum(size(coefs{1}{1,1})>1);
coef_comb = cell(1,nTrees/2);

% do for every batch
for idx=1:nBatches
    
    % concatenate different trees
    coefs_mat = cat(signalDims+1,coefs{idx}{:});
    
    for p=1:nTrees/2
        
        % get weight vector for frequency support
        weight_vector = A(p,:).';
        % add signalDims preceding singleton dimensions
        weight_vector = permute(weight_vector, circshift(1:signalDims+1,-1));
        
        % calc matrix multiplication of coefficient and weight vector
        coef_comb{p} = sum(bsxfun(@times, coefs_mat,weight_vector),signalDims+1);
        
    end
    
    coefs_mat = cat(signalDims+1,coef_comb{:});
    
    % get absolute values
    coefs_abs = 2*coefs_mat.^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);

end


