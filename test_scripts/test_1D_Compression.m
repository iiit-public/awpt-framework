% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_1D_Compression.m:  
%       testing the decomposition, compression and reconstruction of 1D signals using
%       the 1-dimensional AWPs
%       The results are evaluated and compared against the DCT 
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           06.06.2020
%   last Revision:  06.06.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;
figure;
            
for test_case = 1:4
    switch test_case
        case 1
            %% DCT
            try
                clear;
                % test signal chirp
                N = 8192;
                Fs = 1;
                Ts = 1/Fs;
                t = (0:Ts:(N-1)*Ts).';
                signal = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
                
                % transform using the dct
                I_dct = dct(signal);
                
                % sort coefficients by absulte value
                [~,sort_idx] = sort(abs(I_dct),'descend');
                [~,rev_sort_idx] = sort(sort_idx);
                I_dct = I_dct(sort_idx);
                
                % define compression ratios
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                
                % preallocate memory
                SSE_dct = zeros(1,nK);
                PSNR_dct = zeros(1,nK);
                
                for i_K=1:nK
                    % compress signal
                    K=K_vec(i_K);
                    I_dct_comp = I_dct;
                    I_dct_comp(floor(K*N):end) = 0;
                    
                    % inverse sorting
                    I_dct_comp = I_dct_comp(rev_sort_idx);
                    
                    % reconstruction
                    signal_rec = idct(I_dct_comp);
                    
                    % calculate reconstruction error metrics
                    SSE_dct(i_K) = sum((abs(signal - signal_rec)).^2,'all')/...
                        sum((abs(signal)).^2,'all')*100;
                    PSNR_dct(i_K) = 10*log10(1/mean((abs(signal - signal_rec)).^2,'all'));
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_dct);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dct);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DCT Compression: Passed\n');
            catch
                fprintf(2,'DCT Compression: Failed\n');
            end
        case 2
            %% DWT
            try
                clear;
                % test signal
                N = 8192;
                Fs = 1;
                Ts = 1/Fs;
                t = (0:Ts:(N-1)*Ts).';
                signal = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',1,6);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',6,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(signal, a_fil,split_param);
                
                % define compression ratios
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                
                % preallocate memory
                PSNR_dwt = zeros(1,nK);
                SSE_dwt = zeros(1,nK);
                
                for i_K=1:nK
                    
                    % compression
                    K = K_vec(i_K);
                    coef_comp = hard_threshold_compression(coef,round(K*N));
                    
                    % reconstruction
                    signal_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_dwt(i_K) = sum((abs(signal - signal_rec)).^2,'all')/...
                        sum((abs(signal)).^2,'all')*100;
                    PSNR_dwt(i_K) = 10*log10(1/mean((abs(signal - signal_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dwt);hold on
                subplot(2,1,2);
                plot(K_vec,SSE_dwt);hold on
                fprintf('DWT Compression: Passed\n');
            catch
                fprintf(2,'DWT Compression: Failed\n');
            end
            
        case 3
            %% DT-CWT (Real-based compression)
            try
                clear;
                % test signal
                N = 8192;
                Fs = 1;
                Ts = 1/Fs;
                t = (0:Ts:(N-1)*Ts).';
                signal = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',1,6);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',6,1,0,'hwlet',1,2,4,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(signal, a_fil,split_param);
                
                % define compression ratios
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                
                % preallocate memory
                PSNR_dtcwt = zeros(1,nK);
                SSE_dtcwt = zeros(1,nK);
                for i_K=1:nK
                    
                    % compression
                    K = K_vec(i_K);
                    coef_comp = hard_threshold_compression(coef,round(K*N),'real');
                    
                    % reconstruction
                    signal_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_dtcwt(i_K) = sum((abs(signal - signal_rec)).^2,'all')/...
                        sum((abs(signal)).^2,'all')*100;
                    PSNR_dtcwt(i_K) = 10*log10(1/mean((abs(signal - signal_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dtcwt);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dtcwt);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DT-CWT Compression: Passed\n');
            catch
                fprintf(2,'DT-CWT Compression: Failed\n');
            end
        case 4
            %% Signal-adapted WPT
            try
                clear;
                % test signal
                N = 8192;
                Fs = 1;
                Ts = 1/Fs;
                t = (0:Ts:(N-1)*Ts).';
                signal = sin(2*pi*(Fs/(4*8192*Ts)*t).*t);
                signal_energy = signal.'*signal;
                
                % split parameter
                split_param = get_decomposition_structure([],'full_frame',1,9,@hCostEntropyAnalytic,signal_energy,true);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',6,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(signal, a_fil,split_param);
                [coef_list,~] = optimal_basis_search(coef);
                [split_param] = get_decomposition_structure(coef_list,'preset_tree');
                coef = AWPT_analysis(signal, a_fil, split_param);
                
                % define compression ratios
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                
                % preallocate memory
                PSNR_sawpt = zeros(1,nK);
                SSE_sawpt = zeros(1,nK);
                
                for i_K=1:nK
                    
                    % compression
                    K = K_vec(i_K);
                    coef_comp = hard_threshold_compression(coef,round(K*N),'real');
                    
                    % reconstruction
                    signal_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_sawpt(i_K) = sum((abs(signal - signal_rec)).^2,'all')/...
                        sum((abs(signal)).^2,'all')*100;
                    PSNR_sawpt(i_K) = 10*log10(1/mean((abs(signal - signal_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_sawpt);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_sawpt);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('SA-WPT Compression: Passed\n');
            catch
                fprintf(2,'SA-WPT Compression: Failed\n');
            end
    end
end

subplot(2,1,1);
legend('DCT','DWT','DT-CWT','SA-WPT');
subplot(2,1,2);
legend('DCT','DWT','DT-CWT','SA-WPT');



function [cost] = hCostEntropyAnalytic(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);

% do for every batch
for idx=1:nBatches
    

    % concatenate different trees
    coefs_mat = cat(ndims(coefs{idx}{1})+1,coefs{idx}{:});

    % get absolute values
    coefs_abs = sum(coefs_mat.^2,ndims(coefs{idx}{1})+1);
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);


end


function [cost] = hCostEntropyReal(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);
nTrees = length(coefs{1});

% do for every batch
for idx=1:nBatches
    

    % concatenate different trees
    coefs_abs = (nTrees*coefs{idx}{1}).^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);


end


function [cost] = hCostEntropyTotal(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);

% do for every batch
for idx=1:nBatches
    

    % concatenate different trees
    coefs_mat = cat(ndims(coefs{idx}{1})+1,coefs{idx}{:});

    % get absolute values
    coefs_abs = coefs_mat.^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);


end
