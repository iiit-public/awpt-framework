% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_FilterVisualization_2D.m:  
%       In this script, the two methods to visualize the base functions and
%       their spectrum in N dimensions are testet using the special case 2D. 
%       Every base function is completely
%       described using the stage, subband, and time step and 
%       most importantly the filter transfer functions.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           19.12.2019
%   last Revision:  09.06.2020
%
%   The two methods "calc_base_function_MultiDim.m" and 
%   "calc_base_response_MultiDim.m" are tested. 
%   In "calc_base_response_MultiDim" the wavelet filterbank is excited using
%   harmonic signals and the response of the specified coefficient is
%   measured. Therefore, this function ("calc_base_response_MultiDim") only allows 
%   a spectral information and spectral leakage is a problem. To reduce
%   spectral leakage use frequency bins, that fit into the time window N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prepare workspace 
clear;
close all;
clc;

% define base function
coef_index = [3,2;3,2];

%% test real wavelet packets

% filter design real wavelets
[a_fil{1},s_fil{1}] = get_filters('db',4,1,0);
[a_fil{2},s_fil{2}] = get_filters('db',4,1,0);

% calculate base function
try
    calc_base_function_ND(s_fil,coef_index,'analytic',1);
    calc_base_function_ND(s_fil,coef_index,'analytic',2);
    calc_base_function_ND(s_fil,coef_index,'analytic');
    calc_base_function_ND(s_fil,coef_index);
    calc_base_function_ND(s_fil,coef_index,'analytic',1,256);
    calc_base_function_ND(s_fil,coef_index,'real',2);
    [base_WPT] = calc_base_function_ND(s_fil,coef_index,'real',1);

    fprintf('Base function calculation with single tree: Passed\n');
catch
    fprintf(2,'Base function calculation with single tree: Failed\n');
end

% calc frequency response
try
    calc_base_response_ND(a_fil,coef_index,'real',1,64,[-0.4,0,0.2]);
    calc_base_response_ND(a_fil,coef_index,'real',1,60,[-0.4,0,0.2]);
    calc_base_response_ND(a_fil,coef_index,'real',1,16);
    calc_base_response_ND(a_fil,coef_index,'real',2);
    calc_base_response_ND(a_fil,coef_index,'real',1,16,[-0.4,0,0.2]);
    [freq_resp_wpt,f_vec_wpt] = calc_base_response_ND(a_fil,coef_index);
    fprintf('Frequency response calculation: Passed\n');
catch
    fprintf(2,'Frequency response calculation: Failed\n');
end


%% test analytical wavelet packets

% filter design
[a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',1,1,4, 'midphase');
[a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',1,1,4, 'midphase');

% calculate base functions in spectral domain
try
    calc_base_function_ND(s_fil,coef_index,'real',1);
    calc_base_function_ND(s_fil,coef_index,'real',2);
    calc_base_function_ND(s_fil,coef_index,'real',5);
    calc_base_function_ND(s_fil,coef_index,'analytic',1);
    calc_base_function_ND(s_fil,coef_index,'analytic',2);
    calc_base_function_ND(s_fil,coef_index,'analytic',5);
    [base_AWPT] = calc_base_function_ND(s_fil,coef_index,'analytic',5,256);
    
    fprintf('AWPT in real form filter visualization with synthesis method: Passed\n');
catch
    fprintf(2,'AWPT in real form filter visualization with synthesis method: Failed\n');
end

% calc filter response
try
    calc_base_response_ND(a_fil,coef_index,'real',1,64,[-0.4,0,0.2]);
    calc_base_response_ND(a_fil,coef_index,'real',5,32,[-0.4,0,0.2]);
    calc_base_response_ND(a_fil,coef_index,'analytic',2,77,[-0.4,0,0.2]);
    [freq_resp_awpt,f_vec_awpt] = calc_base_response_ND(a_fil,coef_index);
    fprintf('AWPT filter visualization with spectral domain method: Passed\n');
catch
    fprintf(2,'AWPT filter visualization with spectral domain method: Failed\n');
end


%% visualize dual-tree base functions

% define hilbert filter pairs
[a_fil{1},s_fil{1}] = get_filters('db',6,1,0,'hwlet',1,2,5, 'midphase');
[a_fil{2},s_fil{2}] = get_filters('db',6,1,0,'hwlet',1,2,5, 'midphase');

% create figure and define subbands to plot
figure;
subbands = {[3,0;3,0],[3,2;3,0],[3,0;3,2],[3,2;3,2]};

% for both 45 and -45 degree
for supp=1:2
    
    for k=1:length(subbands)
        % set plot axis
        subplot(2,4,(supp-1)*4+k);
        % calculate base function
        [base_AWPT] = calc_base_function_ND(s_fil,subbands{k},'real',supp);
        % plot base
        imagesc(base_AWPT);
    end
end


%% create figures 

% 2D visualization of analytic wavelet packets in spectral domain
hfig1_a = figure;
[Xf,Yf] = meshgrid(f_vec_awpt);
surf(Xf,Yf,abs(freq_resp_awpt));
title('Analytic Wavelet Packets Transfer Function');
xlabel('f/kHz');
ylabel('f/kHz');
zlabel('Spectral density');

% 2D visualization of real wavelet packets in spectral domain
hfig2_a = figure;
[Xf,Yf] = meshgrid(f_vec_wpt);
surf(Xf,Yf,abs(freq_resp_wpt));
title('Real Wavelet Packets Transfer Function');
xlabel('f/kHz');
ylabel('f/kHz');
zlabel('Spectral density');



