% Copyright (C) 2020  The AWP-Framework Authors
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_2D_Compression.m:
%       testing the decomposition, compression and reconstruction of images using
%       the multi-dimensional AWPs
%       The results are evaluated and compared against the DCT
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           26.01.2020
%   last Revision:  03.06.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;
figure;

for test_case = 1:7
    switch test_case
        case 1
            %% DCT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                [Nx, Ny] = size(Image);
                % transform
                I_dct = dct2(Image);
                
                % compression
                I_dct = reshape(I_dct,1,[]);
                N = length(I_dct);
                [~,sort_idx] = sort(abs(I_dct),'descend');
                [~,rev_sort_idx] = sort(sort_idx);
                
                I_dct = I_dct(sort_idx);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                SSE_dct = zeros(1,nK);
                PSNR_dct = zeros(1,nK);
                
                for i_K=1:nK
                    K=K_vec(i_K);
                    I_dct_comp = I_dct;
                    I_dct_comp(floor(K*N):end) = 0;
                    
                    I_dct_comp = I_dct_comp(rev_sort_idx);
                    I_dct_comp = reshape(I_dct_comp,Nx,[]);
                    
                    % reconstruction
                    Image_rec = idct2(I_dct_comp);
                    SSE_dct(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dct(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dct,'DisplayName','DCT');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dct,'DisplayName','DCT');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DCT Compression: Passed\n');
            catch
                fprintf(2,'DCT Compression: Failed\n');
            end
        case 2
            %% DWT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                N = numel(Image);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dwt = zeros(1,nK);
                SSE_dwt = zeros(1,nK);
                for i_K=1:nK
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'single');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_dwt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dwt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_dwt,'DisplayName','DWT');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dwt,'DisplayName','DWT');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DWT Compression: Passed\n');
            catch
                fprintf(2,'DWT Compression: Failed\n');
            end
            
        case 3
            %% DT-CWT (Real-based compression)
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                N = numel(Image);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dtcwt = zeros(1,nK);
                SSE_dtcwt = zeros(1,nK);
                for i_K=1:nK
                    
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'real');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_dtcwt(1,i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dtcwt(1,i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                    
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_dtcwt,'DisplayName','DT-CWT (real-combination)');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dtcwt,'DisplayName','DT-CWT (real-combination)');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DT-CWT Compression (real-combination): Passed\n');
            catch
                fprintf(2,'DT-CWT Compression (real-combination): Failed\n');
            end
        case 4
            %% DT-CWT (analytic value-based compression)
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                N = numel(Image);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dtcwt_analytic = zeros(1,nK);
                SSE_dtcwt_analytic = zeros(1,nK);
                for i_K=1:nK
                    
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'analytic');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_dtcwt_analytic(1,i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dtcwt_analytic(1,i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                    
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_dtcwt_analytic,'DisplayName','DT-CWT (analytic-value)');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dtcwt_analytic,'DisplayName','DT-CWT (analytic-value)');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DT-CWT Compression (analytic-value): Passed\n');
            catch
                fprintf(2,'DT-CWT Compression (analytic-value): Failed\n');
            end
        case 5
            %% DT-CWT (Single tree-based compression)
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                N = numel(Image);
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',1,1,1,'midphase');
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dtcwt_single = zeros(1,nK);
                SSE_dtcwt_single = zeros(1,nK);
                for i_K=1:nK
                    
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'single');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_dtcwt_single(1,i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dtcwt_single(1,i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                    
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_dtcwt_single,'DisplayName','DT-CWT (single tree)');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dtcwt_single,'DisplayName','DT-CWT (single tree)');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DT-CWT Compression (single tree): Passed\n');
            catch
                fprintf(2,'DT-CWT Compression (single tree): Failed\n');
            end
        case 6
            %% Signal-adapted WPT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                signal_energy = sum(Image.^2,'all');
                N = numel(Image);
                
                % split parameter
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropyAllTrees,signal_energy,true);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list,~] = optimal_basis_search(coef);
                [split_param] = get_decomposition_structure(coef_list,'preset_tree');
                coef = AWPT_analysis(Image, a_fil, split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_sawpt = zeros(1,nK);
                SSE_sawpt = zeros(1,nK);
                for i_K=1:nK
                    
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'single');
                    coef_comp = hard_threshold_compression(coef,round(K*N),'real');
                    coef_comp = hard_threshold_compression(coef,round(K*N),'analytic');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_sawpt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_sawpt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_sawpt,'DisplayName','SA-WPT');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_sawpt,'DisplayName','SA-WPT');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('SA-WPT Compression: Passed\n');
            catch
                fprintf(2,'SA-WPT Compression: Failed\n');
            end
        case 7
            %% Signal-adapted AWPT (best basis search optimality criterion)
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                signal_energy = sum(Image.^2,'all');
                N = numel(Image);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',[],1,4);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',[],1,4);
                
                
                % calculate recombination matrix
                nTrees = 4;
                % binary pattern
                Binary_vec = de2bi((0:nTrees-1),'left-msb');
                sign_vec = 1i*(-1).^Binary_vec;
                
                % reshape pattern for efficient calculation of combinatorics multiplication
                % (this add a singleton dimension)
                Binary_vec = permute(Binary_vec,[3,1,2]);
                sign_vec = permute(sign_vec,[1,3,2]);
                
                % perform multiplication along dimension 3
                A_complex = prod(sign_vec.^Binary_vec,3);
                
                % real form
                A = [real(A_complex(1:end/2,:)); imag(A_complex(1:end/2,:))];
                
                % norm matrix to A'*A =  eye(nTrees);
                K = sqrt(trace(A'*A)/nTrees);
                A = 1/K * A;

                
                % get best optimality criterion
                % all trees based entropy
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropyAllTrees,signal_energy,true);
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list{1},entropy{1}] = optimal_basis_search(coef);
                % single tree based entropy
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropySingleTree,signal_energy,true);
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list{2},entropy{2}] = optimal_basis_search(coef);
                % total entropy over all trees
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropyTotal,signal_energy,true);
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list{3},entropy{3}] = optimal_basis_search(coef);
                % entropy of real-valued awp
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropyRealRekombination,{signal_energy,A},true);
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list{3},entropy{4}] = optimal_basis_search(coef);
                
                
                % find best optimality criterion
                [min_entropy,min_idx] = min([entropy{:}]);
                coef_list = coef_list{min_idx};
                
                % perform 'best' decomposition
                [split_param] = get_decomposition_structure(coef_list,'preset_tree');
                coef = AWPT_analysis(Image, a_fil, split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_sawpt = zeros(1,nK);
                SSE_sawpt = zeros(1,nK);
                for i_K=1:nK
                    
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,round(K*N),'single');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    % calc reconstruction metrics
                    SSE_sawpt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_sawpt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                % plot results
                subplot(2,1,1);
                plot(K_vec,PSNR_sawpt,'DisplayName','SA-AWPT (single tree)');hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_sawpt,'DisplayName','SA-AWPT (single tree)');hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('SA-WPT Compression: Passed\n');
            catch
                fprintf(2,'SA-WPT Compression: Failed\n');
            end
    end
end

subplot(2,1,1);
legend
set(gca,'ylim',[15,40]);
subplot(2,1,2);
legend



function [cost] = hCostEntropyAllTrees(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);

% do for every batch
for idx=1:nBatches
    
    % concatenate different trees
    coefs_mat = cat(ndims(coefs{idx}{1})+1,coefs{idx}{:});
    
    % get absolute values
    coefs_abs = sum(coefs_mat.^2,ndims(coefs{idx}{1})+1);
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);

end


function [cost] = hCostEntropySingleTree(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);
nTrees = length(coefs{1});

% do for every batch
for idx=1:nBatches
    
    % concatenate different trees
    coefs_abs = nTrees*(coefs{idx}{1}).^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);

end


function [cost] = hCostEntropyTotal(coefs,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);

% do for every batch
for idx=1:nBatches
    
    % concatenate different trees
    coefs_mat = cat(ndims(coefs{idx}{1})+1,coefs{idx}{:});
    
    % get absolute values
    coefs_abs = coefs_mat.^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);

end


function [cost] = hCostEntropyRealRekombination(coefs,parameters)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% read parameters from input
signal_energy = parameters{1};
A = parameters{2};
nTrees = length(coefs{1});

% preallocate memory
% get number of batches
nBatches = length(coefs);
batch_cost = zeros(1,nBatches);
signalDims = sum(size(coefs{1}{1,1})>1);
coef_comb = cell(1,nTrees/2);

% do for every batch
for idx=1:nBatches
    
    % concatenate different trees
    coefs_mat = cat(signalDims+1,coefs{idx}{:});
    
    for p=1:nTrees/2
        
        % get weight vector for frequency support
        weight_vector = A(p,:).';
        % add signalDims preceding singleton dimensions
        weight_vector = permute(weight_vector, circshift(1:signalDims+1,-1));
        
        % calc matrix multiplication of coefficient and weight vector
        coef_comb{p} = sum(bsxfun(@times, coefs_mat,weight_vector),signalDims+1);
        
    end
    
    coefs_mat = cat(signalDims+1,coef_comb{:});
    
    % get absolute values
    coefs_abs = 2*coefs_mat.^2;
    
    % calculate entropy of signal energy normed coefficients
    batch_cost(1,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),'all','omitnan');
    
end

% the costs of different batches are averaged
cost = mean(batch_cost);

end