% Copyright (C) 2020  The AWP-Framework Authors
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_Filter.m:  testing different filter designs
%
%   Author:         Daniel Schwaer, Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           26.02.2020
%   last Revision:  03.06.2020
%
% This function is the test script for the function get_example_filter and
% different filter combinations
% The results are representated as time-frequency-representation for the
% example filters and as reconstruction errors for the rest of the filters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear;
clc;

%% Defines
N = 8192;
fs = 1;
Ts = 1/fs;
t = (0:Ts:(N-1)*Ts).';

signal = sin(2*pi*(fs/2/(2*N*Ts)*t).*t);
signal = signal./sqrt(sum(signal.^2,1));


%% test Daubechies filters
for n=1:10
    try
        [an_fil,syn_fil] = get_filters('db',n,1,0);
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (db',num2str(n),'): Failed\n']);
    end
end

%% test Fejer-Korovkin filters
for n=2:4
    try
        [an_fil,syn_fil] = get_filters('fk',n*2,1,0);
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Fejer-Korovkin ',num2str(n),'): Failed\n']);
    end
end


%% test Symlets
for n=2:6
    try
        [an_fil,syn_fil] = get_filters('sym',n,1,0);
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Symlets ',num2str(n),'): Failed\n']);
    end
end


%% test Coiflets
for n=1:5
    try
        [an_fil,syn_fil] = get_filters('coif',n,1,0);
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Coiflets ',num2str(n),'): Failed\n']);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         Analytical Wavelet Packets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\nAnalytical Wavelet Packets\n\n')

%% test Daubechies filters
for n=1:10
    try
        [an_fil,syn_fil] = get_filters('db',n,1,0,'hwlet',1,1,4,'midphase');
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (db',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (db',num2str(n),'): Failed\n']);
    end
end

%% test Fejer-Korovkin filters
for n=2:4
    try
        [an_fil,syn_fil] = get_filters('fk',n*2,1,0,'hwlet',1,1,4,'midphase');
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Fejer-Korovkin ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Fejer-Korovkin ',num2str(n),'): Failed\n']);
    end
end


%% test Symlets
for n=2:6
    try
        [an_fil,syn_fil] = get_filters('sym',n,1,0,'hwlet',1,1,4,'midphase');
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Symlets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Symlets ',num2str(n),'): Failed\n']);
    end
end


%% test Coiflets
for n=1:5
    try
        [an_fil,syn_fil] = get_filters('coif',n,1,0,'hwlet',1,1,4,'midphase');
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Coiflets ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Coiflets ',num2str(n),'): Failed\n']);
    end
end

%% test Hwlet filters
for n=1:4
    for k=1:4
        try
            [an_fil,syn_fil] = get_filters('db',2,1,0,'hwlet',1,n,k,'midphase');
            
            % decomposition
            split_param = get_decomposition_structure([], 'full_tree', 1, 6);
            coef = AWPT_analysis(signal, an_fil,split_param);
            
            % reconstruction
            signal_rec = AWPT_synthesis(coef,syn_fil);
            
            ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
            if ReconstructionError<1e-1
                fprintf(['Reconstruction Error (Hwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%%\n']);
            else
                fprintf(2,['Reconstruction Error (Hwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%%\n']);
            end
        catch
            fprintf(2,['Filter (Hwlet ',num2str(n),',',num2str(k),'): Failed\n']);
        end
    end
end

%% test Bwlet filters
for n=2:5
    for k=2:5
        try
            [an_fil,syn_fil] = get_filters('db',2,1,0,'bwlet',1,n,k,'linphase');
            
            % decomposition
            split_param = get_decomposition_structure([], 'full_tree', 1, 6);
            coef = AWPT_analysis(signal, an_fil,split_param);
            
            % reconstruction
            signal_rec = AWPT_synthesis(coef,syn_fil);
            
            ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
            if ReconstructionError<1e-1
                fprintf(['Reconstruction Error (Bwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%%\n']);
            else
                fprintf(2,['Reconstruction Error (Bwlet ',num2str(n),',',num2str(k),'): ',num2str(ReconstructionError),'%%\n']);
            end
        catch
            fprintf(2,['Filter (Bwlet ',num2str(n),',',num2str(k),'): Failed\n']);
        end
    end
end

%% test Qshift filters
for n=4:2:14
    try
        [an_fil,syn_fil] = get_filters('db',2,1,0,'qshift',n,1,1,'midphase');
        
        % decomposition
        split_param = get_decomposition_structure([], 'full_tree', 1, 6);
        coef = AWPT_analysis(signal, an_fil,split_param);
        
        % reconstruction
        signal_rec = AWPT_synthesis(coef,syn_fil);
        
        ReconstructionError = sum(abs(signal-signal_rec).^2,'all')/sum(signal.^2,'all')*100;
        if ReconstructionError<1e-1
            fprintf(['Reconstruction Error (Qshift ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        else
            fprintf(2,['Reconstruction Error (Qshift ',num2str(n),'): ',num2str(ReconstructionError),'%%\n']);
        end
    catch
        fprintf(2,['Filter (Qshift ',num2str(n),'): Failed\n']);
    end
end
